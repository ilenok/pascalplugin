// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface PascalForStatement extends PsiElement {

  @NotNull
  PascalControlVariable getControlVariable();

  @NotNull
  PascalForList getForList();

  @NotNull
  PascalStatement getStatement();

}
