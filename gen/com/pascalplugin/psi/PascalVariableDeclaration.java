// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface PascalVariableDeclaration extends PsiElement {

  @NotNull
  PascalType getType();

  @NotNull
  List<PascalVariableIdentifier> getVariableIdentifierList();

}
