// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface PascalSimpleExpression extends PsiElement {

  @NotNull
  List<PascalAddingOperator> getAddingOperatorList();

  @NotNull
  List<PascalTerm> getTermList();

}
