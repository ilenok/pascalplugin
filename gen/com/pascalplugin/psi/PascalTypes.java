// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import com.pascalplugin.psi.impl.*;

public interface PascalTypes {

  IElementType ACTUAL_PARAMETER = new PascalElementType("ACTUAL_PARAMETER");
  IElementType ADDING_OPERATOR = new PascalElementType("ADDING_OPERATOR");
  IElementType ARRAY_TYPE = new PascalElementType("ARRAY_TYPE");
  IElementType ASSIGNMENT_STATEMENT = new PascalElementType("ASSIGNMENT_STATEMENT");
  IElementType BASE_TYPE = new PascalElementType("BASE_TYPE");
  IElementType CASE_LABEL = new PascalElementType("CASE_LABEL");
  IElementType CASE_LABEL_LIST = new PascalElementType("CASE_LABEL_LIST");
  IElementType CASE_LIST_ELEMENT = new PascalElementType("CASE_LIST_ELEMENT");
  IElementType CASE_STATEMENT = new PascalElementType("CASE_STATEMENT");
  IElementType COMPONENT_TYPE = new PascalElementType("COMPONENT_TYPE");
  IElementType COMPOUND_STATEMENT = new PascalElementType("COMPOUND_STATEMENT");
  IElementType CONDITIONAL_STATEMENT = new PascalElementType("CONDITIONAL_STATEMENT");
  IElementType CONSTANT = new PascalElementType("CONSTANT");
  IElementType CONSTANT_DEFINITION = new PascalElementType("CONSTANT_DEFINITION");
  IElementType CONSTANT_DEFINITION_PART = new PascalElementType("CONSTANT_DEFINITION_PART");
  IElementType CONSTANT_IDENTIFIER = new PascalElementType("CONSTANT_IDENTIFIER");
  IElementType CONTROL_VARIABLE = new PascalElementType("CONTROL_VARIABLE");
  IElementType EXPRESSION = new PascalElementType("EXPRESSION");
  IElementType EXPRESSION_LIST = new PascalElementType("EXPRESSION_LIST");
  IElementType FACTOR = new PascalElementType("FACTOR");
  IElementType FIELD_IDENTIFIER = new PascalElementType("FIELD_IDENTIFIER");
  IElementType FIELD_LIST = new PascalElementType("FIELD_LIST");
  IElementType FILE_TYPE = new PascalElementType("FILE_TYPE");
  IElementType FINAL_VALUE = new PascalElementType("FINAL_VALUE");
  IElementType FIXED_PART = new PascalElementType("FIXED_PART");
  IElementType FORMAL_PARAMETER_SECTION = new PascalElementType("FORMAL_PARAMETER_SECTION");
  IElementType FOR_LIST = new PascalElementType("FOR_LIST");
  IElementType FOR_STATEMENT = new PascalElementType("FOR_STATEMENT");
  IElementType FUNCTION_DECLARATION = new PascalElementType("FUNCTION_DECLARATION");
  IElementType FUNCTION_DESIGNATOR = new PascalElementType("FUNCTION_DESIGNATOR");
  IElementType FUNCTION_HEADING = new PascalElementType("FUNCTION_HEADING");
  IElementType FUNCTION_IDENTIFIER = new PascalElementType("FUNCTION_IDENTIFIER");
  IElementType GOTO_STATEMENT = new PascalElementType("GOTO_STATEMENT");
  IElementType IF_STATEMENT = new PascalElementType("IF_STATEMENT");
  IElementType INDEX_TYPE = new PascalElementType("INDEX_TYPE");
  IElementType INITIAL_VALUE = new PascalElementType("INITIAL_VALUE");
  IElementType LABEL = new PascalElementType("LABEL");
  IElementType LABEL_DECLARATION_PART = new PascalElementType("LABEL_DECLARATION_PART");
  IElementType MULTIPLYING_OPERATOR = new PascalElementType("MULTIPLYING_OPERATOR");
  IElementType PARAMETER_GROUP = new PascalElementType("PARAMETER_GROUP");
  IElementType POINTER_TYPE = new PascalElementType("POINTER_TYPE");
  IElementType PROCEDURE_AND_FUNCTION_DECLARATION_PART = new PascalElementType("PROCEDURE_AND_FUNCTION_DECLARATION_PART");
  IElementType PROCEDURE_DECLARATION = new PascalElementType("PROCEDURE_DECLARATION");
  IElementType PROCEDURE_HEADING = new PascalElementType("PROCEDURE_HEADING");
  IElementType PROCEDURE_IDENTIFIER = new PascalElementType("PROCEDURE_IDENTIFIER");
  IElementType PROCEDURE_OR_FUNCTION_DECLARATION = new PascalElementType("PROCEDURE_OR_FUNCTION_DECLARATION");
  IElementType PROCEDURE_STATEMENT = new PascalElementType("PROCEDURE_STATEMENT");
  IElementType RECORD_SECTION = new PascalElementType("RECORD_SECTION");
  IElementType RECORD_TYPE = new PascalElementType("RECORD_TYPE");
  IElementType RECORD_VARIABLE = new PascalElementType("RECORD_VARIABLE");
  IElementType RELATIONAL_OPERATOR = new PascalElementType("RELATIONAL_OPERATOR");
  IElementType REPEAT_STATEMENT = new PascalElementType("REPEAT_STATEMENT");
  IElementType REPETITIVE_STATEMENT = new PascalElementType("REPETITIVE_STATEMENT");
  IElementType RESULT_TYPE = new PascalElementType("RESULT_TYPE");
  IElementType SCALAR_TYPE = new PascalElementType("SCALAR_TYPE");
  IElementType SET = new PascalElementType("SET");
  IElementType SET_TYPE = new PascalElementType("SET_TYPE");
  IElementType SIMPLE_EXPRESSION = new PascalElementType("SIMPLE_EXPRESSION");
  IElementType SIMPLE_STATEMENT = new PascalElementType("SIMPLE_STATEMENT");
  IElementType SIMPLE_TYPE = new PascalElementType("SIMPLE_TYPE");
  IElementType STATEMENT = new PascalElementType("STATEMENT");
  IElementType STATEMENT_PART = new PascalElementType("STATEMENT_PART");
  IElementType STRUCTURED_STATEMENT = new PascalElementType("STRUCTURED_STATEMENT");
  IElementType STRUCTURED_TYPE = new PascalElementType("STRUCTURED_TYPE");
  IElementType SUBRANGE_TYPE = new PascalElementType("SUBRANGE_TYPE");
  IElementType TAG_FIELD = new PascalElementType("TAG_FIELD");
  IElementType TERM = new PascalElementType("TERM");
  IElementType TYPE = new PascalElementType("TYPE");
  IElementType TYPE_DEFINITION = new PascalElementType("TYPE_DEFINITION");
  IElementType TYPE_DEFINITION_PART = new PascalElementType("TYPE_DEFINITION_PART");
  IElementType TYPE_IDENTIFIER = new PascalElementType("TYPE_IDENTIFIER");
  IElementType UNPACKED_STRUCTURED_TYPE = new PascalElementType("UNPACKED_STRUCTURED_TYPE");
  IElementType VARIABLE = new PascalElementType("VARIABLE");
  IElementType VARIABLE_DECLARATION = new PascalElementType("VARIABLE_DECLARATION");
  IElementType VARIABLE_DECLARATION_PART = new PascalElementType("VARIABLE_DECLARATION_PART");
  IElementType VARIABLE_IDENTIFIER = new PascalElementType("VARIABLE_IDENTIFIER");
  IElementType VARIANT = new PascalElementType("VARIANT");
  IElementType VARIANT_PART = new PascalElementType("VARIANT_PART");
  IElementType WHILE_STATEMENT = new PascalElementType("WHILE_STATEMENT");
  IElementType WITH_STATEMENT = new PascalElementType("WITH_STATEMENT");

  IElementType AND = new PascalTokenType("AND");
  IElementType ARRAY_ = new PascalTokenType("ARRAY_");
  IElementType ASSIGN = new PascalTokenType("ASSIGN");
  IElementType BEGIN_ = new PascalTokenType("BEGIN_");
  IElementType CARET = new PascalTokenType("CARET");
  IElementType CASE = new PascalTokenType("CASE");
  IElementType COLON = new PascalTokenType("COLON");
  IElementType COMMA = new PascalTokenType("COMMA");
  IElementType CONST_ = new PascalTokenType("CONST_");
  IElementType DIV = new PascalTokenType("DIV");
  IElementType DO = new PascalTokenType("DO");
  IElementType DOWNTO = new PascalTokenType("DOWNTO");
  IElementType ELSE = new PascalTokenType("ELSE");
  IElementType END_ = new PascalTokenType("END_");
  IElementType EQUAL_SIGN = new PascalTokenType("EQUAL_SIGN");
  IElementType FILE_ = new PascalTokenType("FILE_");
  IElementType FOR = new PascalTokenType("FOR");
  IElementType FULL_STOP = new PascalTokenType("FULL_STOP");
  IElementType FUNCTION_ = new PascalTokenType("FUNCTION_");
  IElementType GOTO_ = new PascalTokenType("GOTO_");
  IElementType GREATER_OR_EQUAL = new PascalTokenType("GREATER_OR_EQUAL");
  IElementType GREATER_THEN = new PascalTokenType("GREATER_THEN");
  IElementType IDENTIFIER = new PascalTokenType("IDENTIFIER");
  IElementType IF = new PascalTokenType("IF");
  IElementType IN = new PascalTokenType("IN");
  IElementType LABEL_ = new PascalTokenType("LABEL_");
  IElementType LEFT_ROUND_BRACKET = new PascalTokenType("LEFT_ROUND_BRACKET");
  IElementType LEFT_SQUARE_BRACKET = new PascalTokenType("LEFT_SQUARE_BRACKET");
  IElementType LESS_OR_EQUAL = new PascalTokenType("LESS_OR_EQUAL");
  IElementType LESS_THEN = new PascalTokenType("LESS_THEN");
  IElementType MINUS = new PascalTokenType("MINUS");
  IElementType MOD = new PascalTokenType("MOD");
  IElementType MULT = new PascalTokenType("MULT");
  IElementType NIL = new PascalTokenType("NIL");
  IElementType NOT = new PascalTokenType("NOT");
  IElementType OF = new PascalTokenType("OF");
  IElementType OR = new PascalTokenType("OR");
  IElementType PACKED_ = new PascalTokenType("PACKED_");
  IElementType PLUS = new PascalTokenType("PLUS");
  IElementType PROCEDURE_ = new PascalTokenType("PROCEDURE_");
  IElementType RECORD_ = new PascalTokenType("RECORD_");
  IElementType REPEAT = new PascalTokenType("REPEAT");
  IElementType RIGHT_ROUND_BRACKET = new PascalTokenType("RIGHT_ROUND_BRACKET");
  IElementType RIGHT_SQUARE_BRACKET = new PascalTokenType("RIGHT_SQUARE_BRACKET");
  IElementType SEMICOLON = new PascalTokenType("SEMICOLON");
  IElementType SET_ = new PascalTokenType("SET_");
  IElementType SLASH = new PascalTokenType("SLASH");
  IElementType STRING = new PascalTokenType("STRING");
  IElementType THEN = new PascalTokenType("THEN");
  IElementType TO = new PascalTokenType("TO");
  IElementType TYPE_ = new PascalTokenType("TYPE_");
  IElementType UNSIGNED_INTEGER = new PascalTokenType("UNSIGNED_INTEGER");
  IElementType UNSIGNED_REAL = new PascalTokenType("UNSIGNED_REAL");
  IElementType UNTIL = new PascalTokenType("UNTIL");
  IElementType VAR_ = new PascalTokenType("VAR_");
  IElementType WHILE = new PascalTokenType("WHILE");
  IElementType WITH_ = new PascalTokenType("WITH_");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
       if (type == ACTUAL_PARAMETER) {
        return new PascalActualParameterImpl(node);
      }
      else if (type == ADDING_OPERATOR) {
        return new PascalAddingOperatorImpl(node);
      }
      else if (type == ARRAY_TYPE) {
        return new PascalArrayTypeImpl(node);
      }
      else if (type == ASSIGNMENT_STATEMENT) {
        return new PascalAssignmentStatementImpl(node);
      }
      else if (type == BASE_TYPE) {
        return new PascalBaseTypeImpl(node);
      }
      else if (type == CASE_LABEL) {
        return new PascalCaseLabelImpl(node);
      }
      else if (type == CASE_LABEL_LIST) {
        return new PascalCaseLabelListImpl(node);
      }
      else if (type == CASE_LIST_ELEMENT) {
        return new PascalCaseListElementImpl(node);
      }
      else if (type == CASE_STATEMENT) {
        return new PascalCaseStatementImpl(node);
      }
      else if (type == COMPONENT_TYPE) {
        return new PascalComponentTypeImpl(node);
      }
      else if (type == COMPOUND_STATEMENT) {
        return new PascalCompoundStatementImpl(node);
      }
      else if (type == CONDITIONAL_STATEMENT) {
        return new PascalConditionalStatementImpl(node);
      }
      else if (type == CONSTANT) {
        return new PascalConstantImpl(node);
      }
      else if (type == CONSTANT_DEFINITION) {
        return new PascalConstantDefinitionImpl(node);
      }
      else if (type == CONSTANT_DEFINITION_PART) {
        return new PascalConstantDefinitionPartImpl(node);
      }
      else if (type == CONSTANT_IDENTIFIER) {
        return new PascalConstantIdentifierImpl(node);
      }
      else if (type == CONTROL_VARIABLE) {
        return new PascalControlVariableImpl(node);
      }
      else if (type == EXPRESSION) {
        return new PascalExpressionImpl(node);
      }
      else if (type == EXPRESSION_LIST) {
        return new PascalExpressionListImpl(node);
      }
      else if (type == FACTOR) {
        return new PascalFactorImpl(node);
      }
      else if (type == FIELD_IDENTIFIER) {
        return new PascalFieldIdentifierImpl(node);
      }
      else if (type == FIELD_LIST) {
        return new PascalFieldListImpl(node);
      }
      else if (type == FILE_TYPE) {
        return new PascalFileTypeImpl(node);
      }
      else if (type == FINAL_VALUE) {
        return new PascalFinalValueImpl(node);
      }
      else if (type == FIXED_PART) {
        return new PascalFixedPartImpl(node);
      }
      else if (type == FORMAL_PARAMETER_SECTION) {
        return new PascalFormalParameterSectionImpl(node);
      }
      else if (type == FOR_LIST) {
        return new PascalForListImpl(node);
      }
      else if (type == FOR_STATEMENT) {
        return new PascalForStatementImpl(node);
      }
      else if (type == FUNCTION_DECLARATION) {
        return new PascalFunctionDeclarationImpl(node);
      }
      else if (type == FUNCTION_DESIGNATOR) {
        return new PascalFunctionDesignatorImpl(node);
      }
      else if (type == FUNCTION_HEADING) {
        return new PascalFunctionHeadingImpl(node);
      }
      else if (type == FUNCTION_IDENTIFIER) {
        return new PascalFunctionIdentifierImpl(node);
      }
      else if (type == GOTO_STATEMENT) {
        return new PascalGotoStatementImpl(node);
      }
      else if (type == IF_STATEMENT) {
        return new PascalIfStatementImpl(node);
      }
      else if (type == INDEX_TYPE) {
        return new PascalIndexTypeImpl(node);
      }
      else if (type == INITIAL_VALUE) {
        return new PascalInitialValueImpl(node);
      }
      else if (type == LABEL) {
        return new PascalLabelImpl(node);
      }
      else if (type == LABEL_DECLARATION_PART) {
        return new PascalLabelDeclarationPartImpl(node);
      }
      else if (type == MULTIPLYING_OPERATOR) {
        return new PascalMultiplyingOperatorImpl(node);
      }
      else if (type == PARAMETER_GROUP) {
        return new PascalParameterGroupImpl(node);
      }
      else if (type == POINTER_TYPE) {
        return new PascalPointerTypeImpl(node);
      }
      else if (type == PROCEDURE_AND_FUNCTION_DECLARATION_PART) {
        return new PascalProcedureAndFunctionDeclarationPartImpl(node);
      }
      else if (type == PROCEDURE_DECLARATION) {
        return new PascalProcedureDeclarationImpl(node);
      }
      else if (type == PROCEDURE_HEADING) {
        return new PascalProcedureHeadingImpl(node);
      }
      else if (type == PROCEDURE_IDENTIFIER) {
        return new PascalProcedureIdentifierImpl(node);
      }
      else if (type == PROCEDURE_OR_FUNCTION_DECLARATION) {
        return new PascalProcedureOrFunctionDeclarationImpl(node);
      }
      else if (type == PROCEDURE_STATEMENT) {
        return new PascalProcedureStatementImpl(node);
      }
      else if (type == RECORD_SECTION) {
        return new PascalRecordSectionImpl(node);
      }
      else if (type == RECORD_TYPE) {
        return new PascalRecordTypeImpl(node);
      }
      else if (type == RECORD_VARIABLE) {
        return new PascalRecordVariableImpl(node);
      }
      else if (type == RELATIONAL_OPERATOR) {
        return new PascalRelationalOperatorImpl(node);
      }
      else if (type == REPEAT_STATEMENT) {
        return new PascalRepeatStatementImpl(node);
      }
      else if (type == REPETITIVE_STATEMENT) {
        return new PascalRepetitiveStatementImpl(node);
      }
      else if (type == RESULT_TYPE) {
        return new PascalResultTypeImpl(node);
      }
      else if (type == SCALAR_TYPE) {
        return new PascalScalarTypeImpl(node);
      }
      else if (type == SET) {
        return new PascalSetImpl(node);
      }
      else if (type == SET_TYPE) {
        return new PascalSetTypeImpl(node);
      }
      else if (type == SIMPLE_EXPRESSION) {
        return new PascalSimpleExpressionImpl(node);
      }
      else if (type == SIMPLE_STATEMENT) {
        return new PascalSimpleStatementImpl(node);
      }
      else if (type == SIMPLE_TYPE) {
        return new PascalSimpleTypeImpl(node);
      }
      else if (type == STATEMENT) {
        return new PascalStatementImpl(node);
      }
      else if (type == STATEMENT_PART) {
        return new PascalStatementPartImpl(node);
      }
      else if (type == STRUCTURED_STATEMENT) {
        return new PascalStructuredStatementImpl(node);
      }
      else if (type == STRUCTURED_TYPE) {
        return new PascalStructuredTypeImpl(node);
      }
      else if (type == SUBRANGE_TYPE) {
        return new PascalSubrangeTypeImpl(node);
      }
      else if (type == TAG_FIELD) {
        return new PascalTagFieldImpl(node);
      }
      else if (type == TERM) {
        return new PascalTermImpl(node);
      }
      else if (type == TYPE) {
        return new PascalTypeImpl(node);
      }
      else if (type == TYPE_DEFINITION) {
        return new PascalTypeDefinitionImpl(node);
      }
      else if (type == TYPE_DEFINITION_PART) {
        return new PascalTypeDefinitionPartImpl(node);
      }
      else if (type == TYPE_IDENTIFIER) {
        return new PascalTypeIdentifierImpl(node);
      }
      else if (type == UNPACKED_STRUCTURED_TYPE) {
        return new PascalUnpackedStructuredTypeImpl(node);
      }
      else if (type == VARIABLE) {
        return new PascalVariableImpl(node);
      }
      else if (type == VARIABLE_DECLARATION) {
        return new PascalVariableDeclarationImpl(node);
      }
      else if (type == VARIABLE_DECLARATION_PART) {
        return new PascalVariableDeclarationPartImpl(node);
      }
      else if (type == VARIABLE_IDENTIFIER) {
        return new PascalVariableIdentifierImpl(node);
      }
      else if (type == VARIANT) {
        return new PascalVariantImpl(node);
      }
      else if (type == VARIANT_PART) {
        return new PascalVariantPartImpl(node);
      }
      else if (type == WHILE_STATEMENT) {
        return new PascalWhileStatementImpl(node);
      }
      else if (type == WITH_STATEMENT) {
        return new PascalWithStatementImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}
