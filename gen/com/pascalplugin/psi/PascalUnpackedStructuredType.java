// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface PascalUnpackedStructuredType extends PsiElement {

  @Nullable
  PascalArrayType getArrayType();

  @Nullable
  PascalFileType getFileType();

  @Nullable
  PascalRecordType getRecordType();

  @Nullable
  PascalSetType getSetType();

}
