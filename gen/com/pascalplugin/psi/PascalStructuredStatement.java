// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface PascalStructuredStatement extends PsiElement {

  @Nullable
  PascalCompoundStatement getCompoundStatement();

  @Nullable
  PascalConditionalStatement getConditionalStatement();

  @Nullable
  PascalRepetitiveStatement getRepetitiveStatement();

  @Nullable
  PascalWithStatement getWithStatement();

}
