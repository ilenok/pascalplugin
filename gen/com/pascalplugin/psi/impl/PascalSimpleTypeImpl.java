// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.pascalplugin.psi.PascalTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.pascalplugin.psi.*;

public class PascalSimpleTypeImpl extends ASTWrapperPsiElement implements PascalSimpleType {

  public PascalSimpleTypeImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof PascalVisitor) ((PascalVisitor)visitor).visitSimpleType(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public PascalScalarType getScalarType() {
    return findChildByClass(PascalScalarType.class);
  }

  @Override
  @Nullable
  public PascalSubrangeType getSubrangeType() {
    return findChildByClass(PascalSubrangeType.class);
  }

  @Override
  @Nullable
  public PascalTypeIdentifier getTypeIdentifier() {
    return findChildByClass(PascalTypeIdentifier.class);
  }

}
