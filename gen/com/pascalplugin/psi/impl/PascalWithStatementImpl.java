// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.pascalplugin.psi.PascalTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.pascalplugin.psi.*;

public class PascalWithStatementImpl extends ASTWrapperPsiElement implements PascalWithStatement {

  public PascalWithStatementImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof PascalVisitor) ((PascalVisitor)visitor).visitWithStatement(this);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<PascalRecordVariable> getRecordVariableList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, PascalRecordVariable.class);
  }

  @Override
  @NotNull
  public PascalStatement getStatement() {
    return findNotNullChildByClass(PascalStatement.class);
  }

}
