// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.pascalplugin.psi.PascalTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.pascalplugin.psi.*;

public class PascalProcedureDeclarationImpl extends ASTWrapperPsiElement implements PascalProcedureDeclaration {

  public PascalProcedureDeclarationImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof PascalVisitor) ((PascalVisitor)visitor).visitProcedureDeclaration(this);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public PascalConstantDefinitionPart getConstantDefinitionPart() {
    return findNotNullChildByClass(PascalConstantDefinitionPart.class);
  }

  @Override
  @NotNull
  public PascalLabelDeclarationPart getLabelDeclarationPart() {
    return findNotNullChildByClass(PascalLabelDeclarationPart.class);
  }

  @Override
  @NotNull
  public PascalProcedureAndFunctionDeclarationPart getProcedureAndFunctionDeclarationPart() {
    return findNotNullChildByClass(PascalProcedureAndFunctionDeclarationPart.class);
  }

  @Override
  @NotNull
  public PascalProcedureHeading getProcedureHeading() {
    return findNotNullChildByClass(PascalProcedureHeading.class);
  }

  @Override
  @NotNull
  public PascalStatementPart getStatementPart() {
    return findNotNullChildByClass(PascalStatementPart.class);
  }

  @Override
  @NotNull
  public PascalTypeDefinitionPart getTypeDefinitionPart() {
    return findNotNullChildByClass(PascalTypeDefinitionPart.class);
  }

  @Override
  @NotNull
  public PascalVariableDeclarationPart getVariableDeclarationPart() {
    return findNotNullChildByClass(PascalVariableDeclarationPart.class);
  }

}
