// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.pascalplugin.psi.PascalTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.pascalplugin.psi.*;

public class PascalAssignmentStatementImpl extends ASTWrapperPsiElement implements PascalAssignmentStatement {

  public PascalAssignmentStatementImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof PascalVisitor) ((PascalVisitor)visitor).visitAssignmentStatement(this);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public PascalExpression getExpression() {
    return findNotNullChildByClass(PascalExpression.class);
  }

  @Override
  @Nullable
  public PascalFunctionIdentifier getFunctionIdentifier() {
    return findChildByClass(PascalFunctionIdentifier.class);
  }

  @Override
  @Nullable
  public PascalVariable getVariable() {
    return findChildByClass(PascalVariable.class);
  }

}
