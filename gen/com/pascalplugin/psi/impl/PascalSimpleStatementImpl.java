// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.pascalplugin.psi.PascalTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.pascalplugin.psi.*;

public class PascalSimpleStatementImpl extends ASTWrapperPsiElement implements PascalSimpleStatement {

  public PascalSimpleStatementImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof PascalVisitor) ((PascalVisitor)visitor).visitSimpleStatement(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public PascalAssignmentStatement getAssignmentStatement() {
    return findChildByClass(PascalAssignmentStatement.class);
  }

  @Override
  @Nullable
  public PascalGotoStatement getGotoStatement() {
    return findChildByClass(PascalGotoStatement.class);
  }

  @Override
  @Nullable
  public PascalProcedureStatement getProcedureStatement() {
    return findChildByClass(PascalProcedureStatement.class);
  }

}
