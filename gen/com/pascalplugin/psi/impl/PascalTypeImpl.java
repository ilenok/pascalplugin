// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.pascalplugin.psi.PascalTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.pascalplugin.psi.*;

public class PascalTypeImpl extends ASTWrapperPsiElement implements PascalType {

  public PascalTypeImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof PascalVisitor) ((PascalVisitor)visitor).visitType(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public PascalPointerType getPointerType() {
    return findChildByClass(PascalPointerType.class);
  }

  @Override
  @Nullable
  public PascalSimpleType getSimpleType() {
    return findChildByClass(PascalSimpleType.class);
  }

  @Override
  @Nullable
  public PascalStructuredType getStructuredType() {
    return findChildByClass(PascalStructuredType.class);
  }

}
