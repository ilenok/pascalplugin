// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.pascalplugin.psi.PascalTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.pascalplugin.psi.*;

public class PascalVariantPartImpl extends ASTWrapperPsiElement implements PascalVariantPart {

  public PascalVariantPartImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof PascalVisitor) ((PascalVisitor)visitor).visitVariantPart(this);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public PascalTagField getTagField() {
    return findNotNullChildByClass(PascalTagField.class);
  }

  @Override
  @NotNull
  public PascalTypeIdentifier getTypeIdentifier() {
    return findNotNullChildByClass(PascalTypeIdentifier.class);
  }

  @Override
  @NotNull
  public List<PascalVariant> getVariantList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, PascalVariant.class);
  }

}
