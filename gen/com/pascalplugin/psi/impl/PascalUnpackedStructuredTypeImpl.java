// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.pascalplugin.psi.PascalTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.pascalplugin.psi.*;

public class PascalUnpackedStructuredTypeImpl extends ASTWrapperPsiElement implements PascalUnpackedStructuredType {

  public PascalUnpackedStructuredTypeImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof PascalVisitor) ((PascalVisitor)visitor).visitUnpackedStructuredType(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public PascalArrayType getArrayType() {
    return findChildByClass(PascalArrayType.class);
  }

  @Override
  @Nullable
  public PascalFileType getFileType() {
    return findChildByClass(PascalFileType.class);
  }

  @Override
  @Nullable
  public PascalRecordType getRecordType() {
    return findChildByClass(PascalRecordType.class);
  }

  @Override
  @Nullable
  public PascalSetType getSetType() {
    return findChildByClass(PascalSetType.class);
  }

}
