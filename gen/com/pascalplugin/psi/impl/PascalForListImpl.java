// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.pascalplugin.psi.PascalTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.pascalplugin.psi.*;

public class PascalForListImpl extends ASTWrapperPsiElement implements PascalForList {

  public PascalForListImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof PascalVisitor) ((PascalVisitor)visitor).visitForList(this);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public PascalFinalValue getFinalValue() {
    return findNotNullChildByClass(PascalFinalValue.class);
  }

  @Override
  @NotNull
  public PascalInitialValue getInitialValue() {
    return findNotNullChildByClass(PascalInitialValue.class);
  }

}
