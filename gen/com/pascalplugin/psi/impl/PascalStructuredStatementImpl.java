// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.pascalplugin.psi.PascalTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.pascalplugin.psi.*;

public class PascalStructuredStatementImpl extends ASTWrapperPsiElement implements PascalStructuredStatement {

  public PascalStructuredStatementImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof PascalVisitor) ((PascalVisitor)visitor).visitStructuredStatement(this);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public PascalCompoundStatement getCompoundStatement() {
    return findChildByClass(PascalCompoundStatement.class);
  }

  @Override
  @Nullable
  public PascalConditionalStatement getConditionalStatement() {
    return findChildByClass(PascalConditionalStatement.class);
  }

  @Override
  @Nullable
  public PascalRepetitiveStatement getRepetitiveStatement() {
    return findChildByClass(PascalRepetitiveStatement.class);
  }

  @Override
  @Nullable
  public PascalWithStatement getWithStatement() {
    return findChildByClass(PascalWithStatement.class);
  }

}
