// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.pascalplugin.psi.PascalTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.pascalplugin.psi.*;

public class PascalSimpleExpressionImpl extends ASTWrapperPsiElement implements PascalSimpleExpression {

  public PascalSimpleExpressionImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof PascalVisitor) ((PascalVisitor)visitor).visitSimpleExpression(this);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<PascalAddingOperator> getAddingOperatorList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, PascalAddingOperator.class);
  }

  @Override
  @NotNull
  public List<PascalTerm> getTermList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, PascalTerm.class);
  }

}
