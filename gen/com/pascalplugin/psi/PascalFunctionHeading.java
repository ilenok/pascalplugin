// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface PascalFunctionHeading extends PsiElement {

  @NotNull
  List<PascalFormalParameterSection> getFormalParameterSectionList();

  @NotNull
  PascalFunctionIdentifier getFunctionIdentifier();

  @NotNull
  PascalResultType getResultType();

}
