// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface PascalActualParameter extends PsiElement {

  @Nullable
  PascalExpression getExpression();

  @Nullable
  PascalFunctionIdentifier getFunctionIdentifier();

  @Nullable
  PascalProcedureIdentifier getProcedureIdentifier();

  @Nullable
  PascalVariable getVariable();

}
