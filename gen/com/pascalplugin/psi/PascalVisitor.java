// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;

public class PascalVisitor extends PsiElementVisitor {

  public void visitActualParameter(@NotNull PascalActualParameter o) {
    visitPsiElement(o);
  }

  public void visitAddingOperator(@NotNull PascalAddingOperator o) {
    visitPsiElement(o);
  }

  public void visitArrayType(@NotNull PascalArrayType o) {
    visitPsiElement(o);
  }

  public void visitAssignmentStatement(@NotNull PascalAssignmentStatement o) {
    visitPsiElement(o);
  }

  public void visitBaseType(@NotNull PascalBaseType o) {
    visitPsiElement(o);
  }

  public void visitCaseLabel(@NotNull PascalCaseLabel o) {
    visitPsiElement(o);
  }

  public void visitCaseLabelList(@NotNull PascalCaseLabelList o) {
    visitPsiElement(o);
  }

  public void visitCaseListElement(@NotNull PascalCaseListElement o) {
    visitPsiElement(o);
  }

  public void visitCaseStatement(@NotNull PascalCaseStatement o) {
    visitPsiElement(o);
  }

  public void visitComponentType(@NotNull PascalComponentType o) {
    visitPsiElement(o);
  }

  public void visitCompoundStatement(@NotNull PascalCompoundStatement o) {
    visitPsiElement(o);
  }

  public void visitConditionalStatement(@NotNull PascalConditionalStatement o) {
    visitPsiElement(o);
  }

  public void visitConstant(@NotNull PascalConstant o) {
    visitPsiElement(o);
  }

  public void visitConstantDefinition(@NotNull PascalConstantDefinition o) {
    visitPsiElement(o);
  }

  public void visitConstantDefinitionPart(@NotNull PascalConstantDefinitionPart o) {
    visitPsiElement(o);
  }

  public void visitConstantIdentifier(@NotNull PascalConstantIdentifier o) {
    visitPsiElement(o);
  }

  public void visitControlVariable(@NotNull PascalControlVariable o) {
    visitPsiElement(o);
  }

  public void visitExpression(@NotNull PascalExpression o) {
    visitPsiElement(o);
  }

  public void visitExpressionList(@NotNull PascalExpressionList o) {
    visitPsiElement(o);
  }

  public void visitFactor(@NotNull PascalFactor o) {
    visitPsiElement(o);
  }

  public void visitFieldIdentifier(@NotNull PascalFieldIdentifier o) {
    visitPsiElement(o);
  }

  public void visitFieldList(@NotNull PascalFieldList o) {
    visitPsiElement(o);
  }

  public void visitFileType(@NotNull PascalFileType o) {
    visitPsiElement(o);
  }

  public void visitFinalValue(@NotNull PascalFinalValue o) {
    visitPsiElement(o);
  }

  public void visitFixedPart(@NotNull PascalFixedPart o) {
    visitPsiElement(o);
  }

  public void visitForList(@NotNull PascalForList o) {
    visitPsiElement(o);
  }

  public void visitForStatement(@NotNull PascalForStatement o) {
    visitPsiElement(o);
  }

  public void visitFormalParameterSection(@NotNull PascalFormalParameterSection o) {
    visitPsiElement(o);
  }

  public void visitFunctionDeclaration(@NotNull PascalFunctionDeclaration o) {
    visitPsiElement(o);
  }

  public void visitFunctionDesignator(@NotNull PascalFunctionDesignator o) {
    visitPsiElement(o);
  }

  public void visitFunctionHeading(@NotNull PascalFunctionHeading o) {
    visitPsiElement(o);
  }

  public void visitFunctionIdentifier(@NotNull PascalFunctionIdentifier o) {
    visitPsiElement(o);
  }

  public void visitGotoStatement(@NotNull PascalGotoStatement o) {
    visitPsiElement(o);
  }

  public void visitIfStatement(@NotNull PascalIfStatement o) {
    visitPsiElement(o);
  }

  public void visitIndexType(@NotNull PascalIndexType o) {
    visitPsiElement(o);
  }

  public void visitInitialValue(@NotNull PascalInitialValue o) {
    visitPsiElement(o);
  }

  public void visitLabel(@NotNull PascalLabel o) {
    visitPsiElement(o);
  }

  public void visitLabelDeclarationPart(@NotNull PascalLabelDeclarationPart o) {
    visitPsiElement(o);
  }

  public void visitMultiplyingOperator(@NotNull PascalMultiplyingOperator o) {
    visitPsiElement(o);
  }

  public void visitParameterGroup(@NotNull PascalParameterGroup o) {
    visitPsiElement(o);
  }

  public void visitPointerType(@NotNull PascalPointerType o) {
    visitPsiElement(o);
  }

  public void visitProcedureAndFunctionDeclarationPart(@NotNull PascalProcedureAndFunctionDeclarationPart o) {
    visitPsiElement(o);
  }

  public void visitProcedureDeclaration(@NotNull PascalProcedureDeclaration o) {
    visitPsiElement(o);
  }

  public void visitProcedureHeading(@NotNull PascalProcedureHeading o) {
    visitPsiElement(o);
  }

  public void visitProcedureIdentifier(@NotNull PascalProcedureIdentifier o) {
    visitPsiElement(o);
  }

  public void visitProcedureOrFunctionDeclaration(@NotNull PascalProcedureOrFunctionDeclaration o) {
    visitPsiElement(o);
  }

  public void visitProcedureStatement(@NotNull PascalProcedureStatement o) {
    visitPsiElement(o);
  }

  public void visitRecordSection(@NotNull PascalRecordSection o) {
    visitPsiElement(o);
  }

  public void visitRecordType(@NotNull PascalRecordType o) {
    visitPsiElement(o);
  }

  public void visitRecordVariable(@NotNull PascalRecordVariable o) {
    visitPsiElement(o);
  }

  public void visitRelationalOperator(@NotNull PascalRelationalOperator o) {
    visitPsiElement(o);
  }

  public void visitRepeatStatement(@NotNull PascalRepeatStatement o) {
    visitPsiElement(o);
  }

  public void visitRepetitiveStatement(@NotNull PascalRepetitiveStatement o) {
    visitPsiElement(o);
  }

  public void visitResultType(@NotNull PascalResultType o) {
    visitPsiElement(o);
  }

  public void visitScalarType(@NotNull PascalScalarType o) {
    visitPsiElement(o);
  }

  public void visitSet(@NotNull PascalSet o) {
    visitPsiElement(o);
  }

  public void visitSetType(@NotNull PascalSetType o) {
    visitPsiElement(o);
  }

  public void visitSimpleExpression(@NotNull PascalSimpleExpression o) {
    visitPsiElement(o);
  }

  public void visitSimpleStatement(@NotNull PascalSimpleStatement o) {
    visitPsiElement(o);
  }

  public void visitSimpleType(@NotNull PascalSimpleType o) {
    visitPsiElement(o);
  }

  public void visitStatement(@NotNull PascalStatement o) {
    visitPsiElement(o);
  }

  public void visitStatementPart(@NotNull PascalStatementPart o) {
    visitPsiElement(o);
  }

  public void visitStructuredStatement(@NotNull PascalStructuredStatement o) {
    visitPsiElement(o);
  }

  public void visitStructuredType(@NotNull PascalStructuredType o) {
    visitPsiElement(o);
  }

  public void visitSubrangeType(@NotNull PascalSubrangeType o) {
    visitPsiElement(o);
  }

  public void visitTagField(@NotNull PascalTagField o) {
    visitPsiElement(o);
  }

  public void visitTerm(@NotNull PascalTerm o) {
    visitPsiElement(o);
  }

  public void visitType(@NotNull PascalType o) {
    visitPsiElement(o);
  }

  public void visitTypeDefinition(@NotNull PascalTypeDefinition o) {
    visitPsiElement(o);
  }

  public void visitTypeDefinitionPart(@NotNull PascalTypeDefinitionPart o) {
    visitPsiElement(o);
  }

  public void visitTypeIdentifier(@NotNull PascalTypeIdentifier o) {
    visitPsiElement(o);
  }

  public void visitUnpackedStructuredType(@NotNull PascalUnpackedStructuredType o) {
    visitPsiElement(o);
  }

  public void visitVariable(@NotNull PascalVariable o) {
    visitPsiElement(o);
  }

  public void visitVariableDeclaration(@NotNull PascalVariableDeclaration o) {
    visitPsiElement(o);
  }

  public void visitVariableDeclarationPart(@NotNull PascalVariableDeclarationPart o) {
    visitPsiElement(o);
  }

  public void visitVariableIdentifier(@NotNull PascalVariableIdentifier o) {
    visitPsiElement(o);
  }

  public void visitVariant(@NotNull PascalVariant o) {
    visitPsiElement(o);
  }

  public void visitVariantPart(@NotNull PascalVariantPart o) {
    visitPsiElement(o);
  }

  public void visitWhileStatement(@NotNull PascalWhileStatement o) {
    visitPsiElement(o);
  }

  public void visitWithStatement(@NotNull PascalWithStatement o) {
    visitPsiElement(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}
