// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface PascalSimpleType extends PsiElement {

  @Nullable
  PascalScalarType getScalarType();

  @Nullable
  PascalSubrangeType getSubrangeType();

  @Nullable
  PascalTypeIdentifier getTypeIdentifier();

}
