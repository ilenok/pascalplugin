// This is a generated file. Not intended for manual editing.
package com.pascalplugin.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface PascalProcedureDeclaration extends PsiElement {

  @NotNull
  PascalConstantDefinitionPart getConstantDefinitionPart();

  @NotNull
  PascalLabelDeclarationPart getLabelDeclarationPart();

  @NotNull
  PascalProcedureAndFunctionDeclarationPart getProcedureAndFunctionDeclarationPart();

  @NotNull
  PascalProcedureHeading getProcedureHeading();

  @NotNull
  PascalStatementPart getStatementPart();

  @NotNull
  PascalTypeDefinitionPart getTypeDefinitionPart();

  @NotNull
  PascalVariableDeclarationPart getVariableDeclarationPart();

}
