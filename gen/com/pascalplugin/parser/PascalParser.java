// This is a generated file. Not intended for manual editing.
package com.pascalplugin.parser;

import com.intellij.lang.PsiBuilder;
import com.intellij.lang.PsiBuilder.Marker;
import com.intellij.openapi.diagnostic.Logger;
import static com.pascalplugin.psi.PascalTypes.*;
import static com.intellij.lang.parser.GeneratedParserUtilBase.*;
import com.intellij.psi.tree.IElementType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.tree.TokenSet;
import com.intellij.lang.PsiParser;

@SuppressWarnings({"SimplifiableIfStatement", "UnusedAssignment"})
public class PascalParser implements PsiParser {

  public static final Logger LOG_ = Logger.getInstance("com.pascalplugin.parser.PascalParser");

  public ASTNode parse(IElementType root_, PsiBuilder builder_) {
    boolean result_;
    builder_ = adapt_builder_(root_, builder_, this, null);
    Marker marker_ = enter_section_(builder_, 0, _COLLAPSE_, null);
    if (root_ == ACTUAL_PARAMETER) {
      result_ = actual_parameter(builder_, 0);
    }
    else if (root_ == ADDING_OPERATOR) {
      result_ = adding_operator(builder_, 0);
    }
    else if (root_ == ARRAY_TYPE) {
      result_ = array_type(builder_, 0);
    }
    else if (root_ == ASSIGNMENT_STATEMENT) {
      result_ = assignment_statement(builder_, 0);
    }
    else if (root_ == BASE_TYPE) {
      result_ = base_type(builder_, 0);
    }
    else if (root_ == CASE_LABEL) {
      result_ = case_label(builder_, 0);
    }
    else if (root_ == CASE_LABEL_LIST) {
      result_ = case_label_list(builder_, 0);
    }
    else if (root_ == CASE_LIST_ELEMENT) {
      result_ = case_list_element(builder_, 0);
    }
    else if (root_ == CASE_STATEMENT) {
      result_ = case_statement(builder_, 0);
    }
    else if (root_ == COMPONENT_TYPE) {
      result_ = component_type(builder_, 0);
    }
    else if (root_ == COMPOUND_STATEMENT) {
      result_ = compound_statement(builder_, 0);
    }
    else if (root_ == CONDITIONAL_STATEMENT) {
      result_ = conditional_statement(builder_, 0);
    }
    else if (root_ == CONSTANT) {
      result_ = constant(builder_, 0);
    }
    else if (root_ == CONSTANT_DEFINITION) {
      result_ = constant_definition(builder_, 0);
    }
    else if (root_ == CONSTANT_DEFINITION_PART) {
      result_ = constant_definition_part(builder_, 0);
    }
    else if (root_ == CONSTANT_IDENTIFIER) {
      result_ = constant_identifier(builder_, 0);
    }
    else if (root_ == CONTROL_VARIABLE) {
      result_ = control_variable(builder_, 0);
    }
    else if (root_ == EXPRESSION) {
      result_ = expression(builder_, 0);
    }
    else if (root_ == EXPRESSION_LIST) {
      result_ = expression_list(builder_, 0);
    }
    else if (root_ == FACTOR) {
      result_ = factor(builder_, 0);
    }
    else if (root_ == FIELD_IDENTIFIER) {
      result_ = field_identifier(builder_, 0);
    }
    else if (root_ == FIELD_LIST) {
      result_ = field_list(builder_, 0);
    }
    else if (root_ == FILE_TYPE) {
      result_ = file_type(builder_, 0);
    }
    else if (root_ == FINAL_VALUE) {
      result_ = final_value(builder_, 0);
    }
    else if (root_ == FIXED_PART) {
      result_ = fixed_part(builder_, 0);
    }
    else if (root_ == FOR_LIST) {
      result_ = for_list(builder_, 0);
    }
    else if (root_ == FOR_STATEMENT) {
      result_ = for_statement(builder_, 0);
    }
    else if (root_ == FORMAL_PARAMETER_SECTION) {
      result_ = formal_parameter_section(builder_, 0);
    }
    else if (root_ == FUNCTION_DECLARATION) {
      result_ = function_declaration(builder_, 0);
    }
    else if (root_ == FUNCTION_DESIGNATOR) {
      result_ = function_designator(builder_, 0);
    }
    else if (root_ == FUNCTION_HEADING) {
      result_ = function_heading(builder_, 0);
    }
    else if (root_ == FUNCTION_IDENTIFIER) {
      result_ = function_identifier(builder_, 0);
    }
    else if (root_ == GOTO_STATEMENT) {
      result_ = goto_statement(builder_, 0);
    }
    else if (root_ == IF_STATEMENT) {
      result_ = if_statement(builder_, 0);
    }
    else if (root_ == INDEX_TYPE) {
      result_ = index_type(builder_, 0);
    }
    else if (root_ == INITIAL_VALUE) {
      result_ = initial_value(builder_, 0);
    }
    else if (root_ == LABEL) {
      result_ = label(builder_, 0);
    }
    else if (root_ == LABEL_DECLARATION_PART) {
      result_ = label_declaration_part(builder_, 0);
    }
    else if (root_ == MULTIPLYING_OPERATOR) {
      result_ = multiplying_operator(builder_, 0);
    }
    else if (root_ == PARAMETER_GROUP) {
      result_ = parameter_group(builder_, 0);
    }
    else if (root_ == POINTER_TYPE) {
      result_ = pointer_type(builder_, 0);
    }
    else if (root_ == PROCEDURE_AND_FUNCTION_DECLARATION_PART) {
      result_ = procedure_and_function_declaration_part(builder_, 0);
    }
    else if (root_ == PROCEDURE_DECLARATION) {
      result_ = procedure_declaration(builder_, 0);
    }
    else if (root_ == PROCEDURE_HEADING) {
      result_ = procedure_heading(builder_, 0);
    }
    else if (root_ == PROCEDURE_IDENTIFIER) {
      result_ = procedure_identifier(builder_, 0);
    }
    else if (root_ == PROCEDURE_OR_FUNCTION_DECLARATION) {
      result_ = procedure_or_function_declaration(builder_, 0);
    }
    else if (root_ == PROCEDURE_STATEMENT) {
      result_ = procedure_statement(builder_, 0);
    }
    else if (root_ == RECORD_SECTION) {
      result_ = record_section(builder_, 0);
    }
    else if (root_ == RECORD_TYPE) {
      result_ = record_type(builder_, 0);
    }
    else if (root_ == RECORD_VARIABLE) {
      result_ = record_variable(builder_, 0);
    }
    else if (root_ == RELATIONAL_OPERATOR) {
      result_ = relational_operator(builder_, 0);
    }
    else if (root_ == REPEAT_STATEMENT) {
      result_ = repeat_statement(builder_, 0);
    }
    else if (root_ == REPETITIVE_STATEMENT) {
      result_ = repetitive_statement(builder_, 0);
    }
    else if (root_ == RESULT_TYPE) {
      result_ = result_type(builder_, 0);
    }
    else if (root_ == SCALAR_TYPE) {
      result_ = scalar_type(builder_, 0);
    }
    else if (root_ == SET) {
      result_ = set(builder_, 0);
    }
    else if (root_ == SET_TYPE) {
      result_ = set_type(builder_, 0);
    }
    else if (root_ == SIMPLE_EXPRESSION) {
      result_ = simple_expression(builder_, 0);
    }
    else if (root_ == SIMPLE_STATEMENT) {
      result_ = simple_statement(builder_, 0);
    }
    else if (root_ == SIMPLE_TYPE) {
      result_ = simple_type(builder_, 0);
    }
    else if (root_ == STATEMENT) {
      result_ = statement(builder_, 0);
    }
    else if (root_ == STATEMENT_PART) {
      result_ = statement_part(builder_, 0);
    }
    else if (root_ == STRUCTURED_STATEMENT) {
      result_ = structured_statement(builder_, 0);
    }
    else if (root_ == STRUCTURED_TYPE) {
      result_ = structured_type(builder_, 0);
    }
    else if (root_ == SUBRANGE_TYPE) {
      result_ = subrange_type(builder_, 0);
    }
    else if (root_ == TAG_FIELD) {
      result_ = tag_field(builder_, 0);
    }
    else if (root_ == TERM) {
      result_ = term(builder_, 0);
    }
    else if (root_ == TYPE) {
      result_ = type(builder_, 0);
    }
    else if (root_ == TYPE_DEFINITION) {
      result_ = type_definition(builder_, 0);
    }
    else if (root_ == TYPE_DEFINITION_PART) {
      result_ = type_definition_part(builder_, 0);
    }
    else if (root_ == TYPE_IDENTIFIER) {
      result_ = type_identifier(builder_, 0);
    }
    else if (root_ == UNPACKED_STRUCTURED_TYPE) {
      result_ = unpacked_structured_type(builder_, 0);
    }
    else if (root_ == VARIABLE) {
      result_ = variable(builder_, 0);
    }
    else if (root_ == VARIABLE_DECLARATION) {
      result_ = variable_declaration(builder_, 0);
    }
    else if (root_ == VARIABLE_DECLARATION_PART) {
      result_ = variable_declaration_part(builder_, 0);
    }
    else if (root_ == VARIABLE_IDENTIFIER) {
      result_ = variable_identifier(builder_, 0);
    }
    else if (root_ == VARIANT) {
      result_ = variant(builder_, 0);
    }
    else if (root_ == VARIANT_PART) {
      result_ = variant_part(builder_, 0);
    }
    else if (root_ == WHILE_STATEMENT) {
      result_ = while_statement(builder_, 0);
    }
    else if (root_ == WITH_STATEMENT) {
      result_ = with_statement(builder_, 0);
    }
    else {
      result_ = parse_root_(root_, builder_, 0);
    }
    exit_section_(builder_, 0, marker_, root_, result_, true, TRUE_CONDITION);
    return builder_.getTreeBuilt();
  }

  protected boolean parse_root_(final IElementType root_, final PsiBuilder builder_, final int level_) {
    return program(builder_, level_ + 1);
  }

  /* ********************************************************** */
  // expression | variable | procedure_identifier | function_identifier
  public static boolean actual_parameter(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "actual_parameter")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<actual parameter>");
    result_ = expression(builder_, level_ + 1);
    if (!result_) result_ = variable(builder_, level_ + 1);
    if (!result_) result_ = procedure_identifier(builder_, level_ + 1);
    if (!result_) result_ = function_identifier(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, ACTUAL_PARAMETER, result_, false, null);
    return result_;
  }

  /* ********************************************************** */
  // actual_parameter (COMMA actual_parameter)*
  static boolean actual_parameter_list(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "actual_parameter_list")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = actual_parameter(builder_, level_ + 1);
    result_ = result_ && actual_parameter_list_1(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  // (COMMA actual_parameter)*
  private static boolean actual_parameter_list_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "actual_parameter_list_1")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!actual_parameter_list_1_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "actual_parameter_list_1", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // COMMA actual_parameter
  private static boolean actual_parameter_list_1_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "actual_parameter_list_1_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, COMMA);
    result_ = result_ && actual_parameter(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // PLUS | MINUS | OR
  public static boolean adding_operator(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "adding_operator")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<adding operator>");
    result_ = consumeToken(builder_, PLUS);
    if (!result_) result_ = consumeToken(builder_, MINUS);
    if (!result_) result_ = consumeToken(builder_, OR);
    exit_section_(builder_, level_, marker_, ADDING_OPERATOR, result_, false, null);
    return result_;
  }

  /* ********************************************************** */
  // ARRAY_ LEFT_SQUARE_BRACKET index_type (COMMA index_type)* RIGHT_SQUARE_BRACKET
  //     OF component_type
  public static boolean array_type(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "array_type")) return false;
    if (!nextTokenIs(builder_, ARRAY_)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeTokens(builder_, 0, ARRAY_, LEFT_SQUARE_BRACKET);
    result_ = result_ && index_type(builder_, level_ + 1);
    result_ = result_ && array_type_3(builder_, level_ + 1);
    result_ = result_ && consumeTokens(builder_, 0, RIGHT_SQUARE_BRACKET, OF);
    result_ = result_ && component_type(builder_, level_ + 1);
    exit_section_(builder_, marker_, ARRAY_TYPE, result_);
    return result_;
  }

  // (COMMA index_type)*
  private static boolean array_type_3(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "array_type_3")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!array_type_3_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "array_type_3", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // COMMA index_type
  private static boolean array_type_3_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "array_type_3_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, COMMA);
    result_ = result_ && index_type(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // LEFT_SQUARE_BRACKET expression_list RIGHT_SQUARE_BRACKET variable_tail
  static boolean array_variable_tail(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "array_variable_tail")) return false;
    if (!nextTokenIs(builder_, LEFT_SQUARE_BRACKET)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, LEFT_SQUARE_BRACKET);
    result_ = result_ && expression_list(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, RIGHT_SQUARE_BRACKET);
    result_ = result_ && variable_tail(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // variable ASSIGN expression | function_identifier ASSIGN expression
  public static boolean assignment_statement(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "assignment_statement")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = assignment_statement_0(builder_, level_ + 1);
    if (!result_) result_ = assignment_statement_1(builder_, level_ + 1);
    exit_section_(builder_, marker_, ASSIGNMENT_STATEMENT, result_);
    return result_;
  }

  // variable ASSIGN expression
  private static boolean assignment_statement_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "assignment_statement_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = variable(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, ASSIGN);
    result_ = result_ && expression(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  // function_identifier ASSIGN expression
  private static boolean assignment_statement_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "assignment_statement_1")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = function_identifier(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, ASSIGN);
    result_ = result_ && expression(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // simple_type
  public static boolean base_type(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "base_type")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<base type>");
    result_ = simple_type(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, BASE_TYPE, result_, false, null);
    return result_;
  }

  /* ********************************************************** */
  // unsigned_constant
  public static boolean case_label(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "case_label")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<case label>");
    result_ = unsigned_constant(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, CASE_LABEL, result_, false, null);
    return result_;
  }

  /* ********************************************************** */
  // case_label (COMMA case_label)*
  public static boolean case_label_list(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "case_label_list")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<case label list>");
    result_ = case_label(builder_, level_ + 1);
    result_ = result_ && case_label_list_1(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, CASE_LABEL_LIST, result_, false, null);
    return result_;
  }

  // (COMMA case_label)*
  private static boolean case_label_list_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "case_label_list_1")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!case_label_list_1_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "case_label_list_1", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // COMMA case_label
  private static boolean case_label_list_1_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "case_label_list_1_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, COMMA);
    result_ = result_ && case_label(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // case_label_list COLON statement
  public static boolean case_list_element(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "case_list_element")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<case list element>");
    result_ = case_label_list(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, COLON);
    result_ = result_ && statement(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, CASE_LIST_ELEMENT, result_, false, null);
    return result_;
  }

  /* ********************************************************** */
  // CASE expression OF
  //     case_list_element (SEMICOLON case_list_element)* SEMICOLON END_
  public static boolean case_statement(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "case_statement")) return false;
    if (!nextTokenIs(builder_, CASE)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, CASE);
    result_ = result_ && expression(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, OF);
    result_ = result_ && case_list_element(builder_, level_ + 1);
    result_ = result_ && case_statement_4(builder_, level_ + 1);
    result_ = result_ && consumeTokens(builder_, 0, SEMICOLON, END_);
    exit_section_(builder_, marker_, CASE_STATEMENT, result_);
    return result_;
  }

  // (SEMICOLON case_list_element)*
  private static boolean case_statement_4(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "case_statement_4")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!case_statement_4_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "case_statement_4", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // SEMICOLON case_list_element
  private static boolean case_statement_4_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "case_statement_4_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, SEMICOLON);
    result_ = result_ && case_list_element(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // type
  public static boolean component_type(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "component_type")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<component type>");
    result_ = type(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, COMPONENT_TYPE, result_, false, null);
    return result_;
  }

  /* ********************************************************** */
  // BEGIN_ statement (SEMICOLON statement)* END_
  public static boolean compound_statement(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "compound_statement")) return false;
    if (!nextTokenIs(builder_, BEGIN_)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, BEGIN_);
    result_ = result_ && statement(builder_, level_ + 1);
    result_ = result_ && compound_statement_2(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, END_);
    exit_section_(builder_, marker_, COMPOUND_STATEMENT, result_);
    return result_;
  }

  // (SEMICOLON statement)*
  private static boolean compound_statement_2(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "compound_statement_2")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!compound_statement_2_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "compound_statement_2", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // SEMICOLON statement
  private static boolean compound_statement_2_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "compound_statement_2_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, SEMICOLON);
    result_ = result_ && statement(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // if_statement | case_statement
  public static boolean conditional_statement(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "conditional_statement")) return false;
    if (!nextTokenIs(builder_, "<conditional statement>", CASE, IF)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<conditional statement>");
    result_ = if_statement(builder_, level_ + 1);
    if (!result_) result_ = case_statement(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, CONDITIONAL_STATEMENT, result_, false, null);
    return result_;
  }

  /* ********************************************************** */
  // constant_identifier | sign constant_identifier
  //     | unsigned_number| sign unsigned_number | STRING
  public static boolean constant(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "constant")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<constant>");
    result_ = constant_identifier(builder_, level_ + 1);
    if (!result_) result_ = constant_1(builder_, level_ + 1);
    if (!result_) result_ = unsigned_number(builder_, level_ + 1);
    if (!result_) result_ = constant_3(builder_, level_ + 1);
    if (!result_) result_ = consumeToken(builder_, STRING);
    exit_section_(builder_, level_, marker_, CONSTANT, result_, false, null);
    return result_;
  }

  // sign constant_identifier
  private static boolean constant_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "constant_1")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = sign(builder_, level_ + 1);
    result_ = result_ && constant_identifier(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  // sign unsigned_number
  private static boolean constant_3(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "constant_3")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = sign(builder_, level_ + 1);
    result_ = result_ && unsigned_number(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // constant_identifier EQUAL_SIGN constant
  public static boolean constant_definition(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "constant_definition")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = constant_identifier(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, EQUAL_SIGN);
    result_ = result_ && constant(builder_, level_ + 1);
    exit_section_(builder_, marker_, CONSTANT_DEFINITION, result_);
    return result_;
  }

  /* ********************************************************** */
  // constant_definition (SEMICOLON constant_definition)*
  static boolean constant_definition_list(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "constant_definition_list")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = constant_definition(builder_, level_ + 1);
    result_ = result_ && constant_definition_list_1(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  // (SEMICOLON constant_definition)*
  private static boolean constant_definition_list_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "constant_definition_list_1")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!constant_definition_list_1_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "constant_definition_list_1", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // SEMICOLON constant_definition
  private static boolean constant_definition_list_1_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "constant_definition_list_1_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, SEMICOLON);
    result_ = result_ && constant_definition(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // CONST_ constant_definition_list SEMICOLON | empty
  public static boolean constant_definition_part(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "constant_definition_part")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<constant definition part>");
    result_ = constant_definition_part_0(builder_, level_ + 1);
    if (!result_) result_ = empty(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, CONSTANT_DEFINITION_PART, result_, false, null);
    return result_;
  }

  // CONST_ constant_definition_list SEMICOLON
  private static boolean constant_definition_part_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "constant_definition_part_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, CONST_);
    result_ = result_ && constant_definition_list(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, SEMICOLON);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean constant_identifier(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "constant_identifier")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, IDENTIFIER);
    exit_section_(builder_, marker_, CONSTANT_IDENTIFIER, result_);
    return result_;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean control_variable(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "control_variable")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, IDENTIFIER);
    exit_section_(builder_, marker_, CONTROL_VARIABLE, result_);
    return result_;
  }

  /* ********************************************************** */
  static boolean empty(PsiBuilder builder_, int level_) {
    return true;
  }

  /* ********************************************************** */
  // simple_expression relational_operator simple_expression
  //     | simple_expression
  public static boolean expression(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "expression")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<expression>");
    result_ = expression_0(builder_, level_ + 1);
    if (!result_) result_ = simple_expression(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, EXPRESSION, result_, false, null);
    return result_;
  }

  // simple_expression relational_operator simple_expression
  private static boolean expression_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "expression_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = simple_expression(builder_, level_ + 1);
    result_ = result_ && relational_operator(builder_, level_ + 1);
    result_ = result_ && simple_expression(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // expression (COMMA expression)*
  public static boolean expression_list(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "expression_list")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<expression list>");
    result_ = expression(builder_, level_ + 1);
    result_ = result_ && expression_list_1(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, EXPRESSION_LIST, result_, false, null);
    return result_;
  }

  // (COMMA expression)*
  private static boolean expression_list_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "expression_list_1")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!expression_list_1_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "expression_list_1", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // COMMA expression
  private static boolean expression_list_1_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "expression_list_1_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, COMMA);
    result_ = result_ && expression(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // function_designator | variable
  //                 | set | LEFT_ROUND_BRACKET expression RIGHT_ROUND_BRACKET
  //                 | unsigned_constant |  NOT factor
  public static boolean factor(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "factor")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<factor>");
    result_ = function_designator(builder_, level_ + 1);
    if (!result_) result_ = variable(builder_, level_ + 1);
    if (!result_) result_ = set(builder_, level_ + 1);
    if (!result_) result_ = factor_3(builder_, level_ + 1);
    if (!result_) result_ = unsigned_constant(builder_, level_ + 1);
    if (!result_) result_ = factor_5(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, FACTOR, result_, false, null);
    return result_;
  }

  // LEFT_ROUND_BRACKET expression RIGHT_ROUND_BRACKET
  private static boolean factor_3(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "factor_3")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, LEFT_ROUND_BRACKET);
    result_ = result_ && expression(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, RIGHT_ROUND_BRACKET);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  // NOT factor
  private static boolean factor_5(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "factor_5")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, NOT);
    result_ = result_ && factor(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // FULL_STOP field_identifier variable_tail
  static boolean field_designator_tail(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "field_designator_tail")) return false;
    if (!nextTokenIs(builder_, FULL_STOP)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, FULL_STOP);
    result_ = result_ && field_identifier(builder_, level_ + 1);
    result_ = result_ && variable_tail(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean field_identifier(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "field_identifier")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, IDENTIFIER);
    exit_section_(builder_, marker_, FIELD_IDENTIFIER, result_);
    return result_;
  }

  /* ********************************************************** */
  // fixed_part | fixed_part SEMICOLON variant_part
  //     | variant_part
  public static boolean field_list(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "field_list")) return false;
    if (!nextTokenIs(builder_, "<field list>", CASE, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<field list>");
    result_ = fixed_part(builder_, level_ + 1);
    if (!result_) result_ = field_list_1(builder_, level_ + 1);
    if (!result_) result_ = variant_part(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, FIELD_LIST, result_, false, null);
    return result_;
  }

  // fixed_part SEMICOLON variant_part
  private static boolean field_list_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "field_list_1")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = fixed_part(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, SEMICOLON);
    result_ = result_ && variant_part(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // FILE_ OF type
  public static boolean file_type(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "file_type")) return false;
    if (!nextTokenIs(builder_, FILE_)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeTokens(builder_, 0, FILE_, OF);
    result_ = result_ && type(builder_, level_ + 1);
    exit_section_(builder_, marker_, FILE_TYPE, result_);
    return result_;
  }

  /* ********************************************************** */
  // expression
  public static boolean final_value(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "final_value")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<final value>");
    result_ = expression(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, FINAL_VALUE, result_, false, null);
    return result_;
  }

  /* ********************************************************** */
  // record_section (SEMICOLON record_section)*
  public static boolean fixed_part(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "fixed_part")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = record_section(builder_, level_ + 1);
    result_ = result_ && fixed_part_1(builder_, level_ + 1);
    exit_section_(builder_, marker_, FIXED_PART, result_);
    return result_;
  }

  // (SEMICOLON record_section)*
  private static boolean fixed_part_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "fixed_part_1")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!fixed_part_1_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "fixed_part_1", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // SEMICOLON record_section
  private static boolean fixed_part_1_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "fixed_part_1_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, SEMICOLON);
    result_ = result_ && record_section(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // initial_value TO final_value
  //     | initial_value DOWNTO final_value
  public static boolean for_list(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "for_list")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<for list>");
    result_ = for_list_0(builder_, level_ + 1);
    if (!result_) result_ = for_list_1(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, FOR_LIST, result_, false, null);
    return result_;
  }

  // initial_value TO final_value
  private static boolean for_list_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "for_list_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = initial_value(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, TO);
    result_ = result_ && final_value(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  // initial_value DOWNTO final_value
  private static boolean for_list_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "for_list_1")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = initial_value(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, DOWNTO);
    result_ = result_ && final_value(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // FOR control_variable ASSIGN for_list DO statement
  public static boolean for_statement(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "for_statement")) return false;
    if (!nextTokenIs(builder_, FOR)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, FOR);
    result_ = result_ && control_variable(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, ASSIGN);
    result_ = result_ && for_list(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, DO);
    result_ = result_ && statement(builder_, level_ + 1);
    exit_section_(builder_, marker_, FOR_STATEMENT, result_);
    return result_;
  }

  /* ********************************************************** */
  // parameter_group | VAR_ parameter_group |
  //     FUNCTION_ parameter_group | PROCEDURE_ IDENTIFIER (COMMA IDENTIFIER)*
  public static boolean formal_parameter_section(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "formal_parameter_section")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<formal parameter section>");
    result_ = parameter_group(builder_, level_ + 1);
    if (!result_) result_ = formal_parameter_section_1(builder_, level_ + 1);
    if (!result_) result_ = formal_parameter_section_2(builder_, level_ + 1);
    if (!result_) result_ = formal_parameter_section_3(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, FORMAL_PARAMETER_SECTION, result_, false, null);
    return result_;
  }

  // VAR_ parameter_group
  private static boolean formal_parameter_section_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "formal_parameter_section_1")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, VAR_);
    result_ = result_ && parameter_group(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  // FUNCTION_ parameter_group
  private static boolean formal_parameter_section_2(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "formal_parameter_section_2")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, FUNCTION_);
    result_ = result_ && parameter_group(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  // PROCEDURE_ IDENTIFIER (COMMA IDENTIFIER)*
  private static boolean formal_parameter_section_3(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "formal_parameter_section_3")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeTokens(builder_, 0, PROCEDURE_, IDENTIFIER);
    result_ = result_ && formal_parameter_section_3_2(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  // (COMMA IDENTIFIER)*
  private static boolean formal_parameter_section_3_2(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "formal_parameter_section_3_2")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!formal_parameter_section_3_2_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "formal_parameter_section_3_2", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // COMMA IDENTIFIER
  private static boolean formal_parameter_section_3_2_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "formal_parameter_section_3_2_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeTokens(builder_, 0, COMMA, IDENTIFIER);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // function_heading label_declaration_part constant_definition_part
  //     type_definition_part variable_declaration_part procedure_and_function_declaration_part
  //     statement_part
  public static boolean function_declaration(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "function_declaration")) return false;
    if (!nextTokenIs(builder_, FUNCTION_)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = function_heading(builder_, level_ + 1);
    result_ = result_ && label_declaration_part(builder_, level_ + 1);
    result_ = result_ && constant_definition_part(builder_, level_ + 1);
    result_ = result_ && type_definition_part(builder_, level_ + 1);
    result_ = result_ && variable_declaration_part(builder_, level_ + 1);
    result_ = result_ && procedure_and_function_declaration_part(builder_, level_ + 1);
    result_ = result_ && statement_part(builder_, level_ + 1);
    exit_section_(builder_, marker_, FUNCTION_DECLARATION, result_);
    return result_;
  }

  /* ********************************************************** */
  // function_identifier LEFT_ROUND_BRACKET actual_parameter (COMMA actual_parameter)* RIGHT_ROUND_BRACKET
  //     | function_identifier
  public static boolean function_designator(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "function_designator")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = function_designator_0(builder_, level_ + 1);
    if (!result_) result_ = function_identifier(builder_, level_ + 1);
    exit_section_(builder_, marker_, FUNCTION_DESIGNATOR, result_);
    return result_;
  }

  // function_identifier LEFT_ROUND_BRACKET actual_parameter (COMMA actual_parameter)* RIGHT_ROUND_BRACKET
  private static boolean function_designator_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "function_designator_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = function_identifier(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, LEFT_ROUND_BRACKET);
    result_ = result_ && actual_parameter(builder_, level_ + 1);
    result_ = result_ && function_designator_0_3(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, RIGHT_ROUND_BRACKET);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  // (COMMA actual_parameter)*
  private static boolean function_designator_0_3(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "function_designator_0_3")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!function_designator_0_3_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "function_designator_0_3", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // COMMA actual_parameter
  private static boolean function_designator_0_3_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "function_designator_0_3_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, COMMA);
    result_ = result_ && actual_parameter(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // FUNCTION_ function_identifier COLON result_type SEMICOLON |
  //     FUNCTION_ function_identifier LEFT_ROUND_BRACKET formal_parameter_section
  //     (SEMICOLON formal_parameter_section)* RIGHT_ROUND_BRACKET COLON result_type SEMICOLON
  public static boolean function_heading(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "function_heading")) return false;
    if (!nextTokenIs(builder_, FUNCTION_)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = function_heading_0(builder_, level_ + 1);
    if (!result_) result_ = function_heading_1(builder_, level_ + 1);
    exit_section_(builder_, marker_, FUNCTION_HEADING, result_);
    return result_;
  }

  // FUNCTION_ function_identifier COLON result_type SEMICOLON
  private static boolean function_heading_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "function_heading_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, FUNCTION_);
    result_ = result_ && function_identifier(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, COLON);
    result_ = result_ && result_type(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, SEMICOLON);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  // FUNCTION_ function_identifier LEFT_ROUND_BRACKET formal_parameter_section
  //     (SEMICOLON formal_parameter_section)* RIGHT_ROUND_BRACKET COLON result_type SEMICOLON
  private static boolean function_heading_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "function_heading_1")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, FUNCTION_);
    result_ = result_ && function_identifier(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, LEFT_ROUND_BRACKET);
    result_ = result_ && formal_parameter_section(builder_, level_ + 1);
    result_ = result_ && function_heading_1_4(builder_, level_ + 1);
    result_ = result_ && consumeTokens(builder_, 0, RIGHT_ROUND_BRACKET, COLON);
    result_ = result_ && result_type(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, SEMICOLON);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  // (SEMICOLON formal_parameter_section)*
  private static boolean function_heading_1_4(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "function_heading_1_4")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!function_heading_1_4_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "function_heading_1_4", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // SEMICOLON formal_parameter_section
  private static boolean function_heading_1_4_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "function_heading_1_4_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, SEMICOLON);
    result_ = result_ && formal_parameter_section(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean function_identifier(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "function_identifier")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, IDENTIFIER);
    exit_section_(builder_, marker_, FUNCTION_IDENTIFIER, result_);
    return result_;
  }

  /* ********************************************************** */
  // GOTO_ label
  public static boolean goto_statement(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "goto_statement")) return false;
    if (!nextTokenIs(builder_, GOTO_)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, GOTO_);
    result_ = result_ && label(builder_, level_ + 1);
    exit_section_(builder_, marker_, GOTO_STATEMENT, result_);
    return result_;
  }

  /* ********************************************************** */
  // IF expression THEN statement ELSE statement
  //     | IF expression THEN statement
  public static boolean if_statement(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "if_statement")) return false;
    if (!nextTokenIs(builder_, IF)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = if_statement_0(builder_, level_ + 1);
    if (!result_) result_ = if_statement_1(builder_, level_ + 1);
    exit_section_(builder_, marker_, IF_STATEMENT, result_);
    return result_;
  }

  // IF expression THEN statement ELSE statement
  private static boolean if_statement_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "if_statement_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, IF);
    result_ = result_ && expression(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, THEN);
    result_ = result_ && statement(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, ELSE);
    result_ = result_ && statement(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  // IF expression THEN statement
  private static boolean if_statement_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "if_statement_1")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, IF);
    result_ = result_ && expression(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, THEN);
    result_ = result_ && statement(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // simple_type
  public static boolean index_type(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "index_type")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<index type>");
    result_ = simple_type(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, INDEX_TYPE, result_, false, null);
    return result_;
  }

  /* ********************************************************** */
  // expression
  public static boolean initial_value(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "initial_value")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<initial value>");
    result_ = expression(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, INITIAL_VALUE, result_, false, null);
    return result_;
  }

  /* ********************************************************** */
  // UNSIGNED_INTEGER
  public static boolean label(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "label")) return false;
    if (!nextTokenIs(builder_, UNSIGNED_INTEGER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, UNSIGNED_INTEGER);
    exit_section_(builder_, marker_, LABEL, result_);
    return result_;
  }

  /* ********************************************************** */
  // LABEL_ label_list SEMICOLON | empty
  public static boolean label_declaration_part(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "label_declaration_part")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<label declaration part>");
    result_ = label_declaration_part_0(builder_, level_ + 1);
    if (!result_) result_ = empty(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, LABEL_DECLARATION_PART, result_, false, null);
    return result_;
  }

  // LABEL_ label_list SEMICOLON
  private static boolean label_declaration_part_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "label_declaration_part_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, LABEL_);
    result_ = result_ && label_list(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, SEMICOLON);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // label (COMMA label)*
  static boolean label_list(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "label_list")) return false;
    if (!nextTokenIs(builder_, UNSIGNED_INTEGER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = label(builder_, level_ + 1);
    result_ = result_ && label_list_1(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  // (COMMA label)*
  private static boolean label_list_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "label_list_1")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!label_list_1_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "label_list_1", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // COMMA label
  private static boolean label_list_1_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "label_list_1_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, COMMA);
    result_ = result_ && label(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // MULT | SLASH | DIV | MOD | AND
  public static boolean multiplying_operator(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "multiplying_operator")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<multiplying operator>");
    result_ = consumeToken(builder_, MULT);
    if (!result_) result_ = consumeToken(builder_, SLASH);
    if (!result_) result_ = consumeToken(builder_, DIV);
    if (!result_) result_ = consumeToken(builder_, MOD);
    if (!result_) result_ = consumeToken(builder_, AND);
    exit_section_(builder_, level_, marker_, MULTIPLYING_OPERATOR, result_, false, null);
    return result_;
  }

  /* ********************************************************** */
  // IDENTIFIER (COMMA IDENTIFIER)* COLON type_identifier
  public static boolean parameter_group(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "parameter_group")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, IDENTIFIER);
    result_ = result_ && parameter_group_1(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, COLON);
    result_ = result_ && type_identifier(builder_, level_ + 1);
    exit_section_(builder_, marker_, PARAMETER_GROUP, result_);
    return result_;
  }

  // (COMMA IDENTIFIER)*
  private static boolean parameter_group_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "parameter_group_1")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!parameter_group_1_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "parameter_group_1", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // COMMA IDENTIFIER
  private static boolean parameter_group_1_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "parameter_group_1_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeTokens(builder_, 0, COMMA, IDENTIFIER);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // CARET type_identifier
  public static boolean pointer_type(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "pointer_type")) return false;
    if (!nextTokenIs(builder_, CARET)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, CARET);
    result_ = result_ && type_identifier(builder_, level_ + 1);
    exit_section_(builder_, marker_, POINTER_TYPE, result_);
    return result_;
  }

  /* ********************************************************** */
  // (procedure_or_function_declaration SEMICOLON)*
  public static boolean procedure_and_function_declaration_part(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "procedure_and_function_declaration_part")) return false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<procedure and function declaration part>");
    int pos_ = current_position_(builder_);
    while (true) {
      if (!procedure_and_function_declaration_part_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "procedure_and_function_declaration_part", pos_)) break;
      pos_ = current_position_(builder_);
    }
    exit_section_(builder_, level_, marker_, PROCEDURE_AND_FUNCTION_DECLARATION_PART, true, false, null);
    return true;
  }

  // procedure_or_function_declaration SEMICOLON
  private static boolean procedure_and_function_declaration_part_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "procedure_and_function_declaration_part_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = procedure_or_function_declaration(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, SEMICOLON);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // procedure_heading label_declaration_part constant_definition_part
  //     type_definition_part variable_declaration_part procedure_and_function_declaration_part
  //     statement_part
  public static boolean procedure_declaration(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "procedure_declaration")) return false;
    if (!nextTokenIs(builder_, PROCEDURE_)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = procedure_heading(builder_, level_ + 1);
    result_ = result_ && label_declaration_part(builder_, level_ + 1);
    result_ = result_ && constant_definition_part(builder_, level_ + 1);
    result_ = result_ && type_definition_part(builder_, level_ + 1);
    result_ = result_ && variable_declaration_part(builder_, level_ + 1);
    result_ = result_ && procedure_and_function_declaration_part(builder_, level_ + 1);
    result_ = result_ && statement_part(builder_, level_ + 1);
    exit_section_(builder_, marker_, PROCEDURE_DECLARATION, result_);
    return result_;
  }

  /* ********************************************************** */
  // PROCEDURE_ procedure_identifier SEMICOLON |
  //     PROCEDURE_ procedure_identifier LEFT_ROUND_BRACKET formal_parameter_section
  //     (SEMICOLON formal_parameter_section)* RIGHT_ROUND_BRACKET SEMICOLON
  public static boolean procedure_heading(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "procedure_heading")) return false;
    if (!nextTokenIs(builder_, PROCEDURE_)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = procedure_heading_0(builder_, level_ + 1);
    if (!result_) result_ = procedure_heading_1(builder_, level_ + 1);
    exit_section_(builder_, marker_, PROCEDURE_HEADING, result_);
    return result_;
  }

  // PROCEDURE_ procedure_identifier SEMICOLON
  private static boolean procedure_heading_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "procedure_heading_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, PROCEDURE_);
    result_ = result_ && procedure_identifier(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, SEMICOLON);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  // PROCEDURE_ procedure_identifier LEFT_ROUND_BRACKET formal_parameter_section
  //     (SEMICOLON formal_parameter_section)* RIGHT_ROUND_BRACKET SEMICOLON
  private static boolean procedure_heading_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "procedure_heading_1")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, PROCEDURE_);
    result_ = result_ && procedure_identifier(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, LEFT_ROUND_BRACKET);
    result_ = result_ && formal_parameter_section(builder_, level_ + 1);
    result_ = result_ && procedure_heading_1_4(builder_, level_ + 1);
    result_ = result_ && consumeTokens(builder_, 0, RIGHT_ROUND_BRACKET, SEMICOLON);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  // (SEMICOLON formal_parameter_section)*
  private static boolean procedure_heading_1_4(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "procedure_heading_1_4")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!procedure_heading_1_4_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "procedure_heading_1_4", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // SEMICOLON formal_parameter_section
  private static boolean procedure_heading_1_4_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "procedure_heading_1_4_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, SEMICOLON);
    result_ = result_ && formal_parameter_section(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean procedure_identifier(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "procedure_identifier")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, IDENTIFIER);
    exit_section_(builder_, marker_, PROCEDURE_IDENTIFIER, result_);
    return result_;
  }

  /* ********************************************************** */
  // procedure_declaration | function_declaration
  public static boolean procedure_or_function_declaration(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "procedure_or_function_declaration")) return false;
    if (!nextTokenIs(builder_, "<procedure or function declaration>", FUNCTION_, PROCEDURE_)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<procedure or function declaration>");
    result_ = procedure_declaration(builder_, level_ + 1);
    if (!result_) result_ = function_declaration(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, PROCEDURE_OR_FUNCTION_DECLARATION, result_, false, null);
    return result_;
  }

  /* ********************************************************** */
  // procedure_identifier LEFT_ROUND_BRACKET actual_parameter_list RIGHT_ROUND_BRACKET
  //     | procedure_identifier
  public static boolean procedure_statement(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "procedure_statement")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = procedure_statement_0(builder_, level_ + 1);
    if (!result_) result_ = procedure_identifier(builder_, level_ + 1);
    exit_section_(builder_, marker_, PROCEDURE_STATEMENT, result_);
    return result_;
  }

  // procedure_identifier LEFT_ROUND_BRACKET actual_parameter_list RIGHT_ROUND_BRACKET
  private static boolean procedure_statement_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "procedure_statement_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = procedure_identifier(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, LEFT_ROUND_BRACKET);
    result_ = result_ && actual_parameter_list(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, RIGHT_ROUND_BRACKET);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // label_declaration_part constant_definition_part type_definition_part
  //     variable_declaration_part procedure_and_function_declaration_part
  //     statement_part FULL_STOP
  static boolean program(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "program")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = label_declaration_part(builder_, level_ + 1);
    result_ = result_ && constant_definition_part(builder_, level_ + 1);
    result_ = result_ && type_definition_part(builder_, level_ + 1);
    result_ = result_ && variable_declaration_part(builder_, level_ + 1);
    result_ = result_ && procedure_and_function_declaration_part(builder_, level_ + 1);
    result_ = result_ && statement_part(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, FULL_STOP);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // field_identifier (SEMICOLON field_identifier)* COLON type
  public static boolean record_section(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "record_section")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = field_identifier(builder_, level_ + 1);
    result_ = result_ && record_section_1(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, COLON);
    result_ = result_ && type(builder_, level_ + 1);
    exit_section_(builder_, marker_, RECORD_SECTION, result_);
    return result_;
  }

  // (SEMICOLON field_identifier)*
  private static boolean record_section_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "record_section_1")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!record_section_1_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "record_section_1", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // SEMICOLON field_identifier
  private static boolean record_section_1_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "record_section_1_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, SEMICOLON);
    result_ = result_ && field_identifier(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // RECORD_ field_list END_
  public static boolean record_type(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "record_type")) return false;
    if (!nextTokenIs(builder_, RECORD_)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, RECORD_);
    result_ = result_ && field_list(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, END_);
    exit_section_(builder_, marker_, RECORD_TYPE, result_);
    return result_;
  }

  /* ********************************************************** */
  // variable
  public static boolean record_variable(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "record_variable")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = variable(builder_, level_ + 1);
    exit_section_(builder_, marker_, RECORD_VARIABLE, result_);
    return result_;
  }

  /* ********************************************************** */
  // record_variable (COMMA record_variable)*
  static boolean record_variable_list(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "record_variable_list")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = record_variable(builder_, level_ + 1);
    result_ = result_ && record_variable_list_1(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  // (COMMA record_variable)*
  private static boolean record_variable_list_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "record_variable_list_1")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!record_variable_list_1_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "record_variable_list_1", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // COMMA record_variable
  private static boolean record_variable_list_1_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "record_variable_list_1_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, COMMA);
    result_ = result_ && record_variable(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // CARET variable_tail
  static boolean referenced_variable_tail(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "referenced_variable_tail")) return false;
    if (!nextTokenIs(builder_, CARET)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, CARET);
    result_ = result_ && variable_tail(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // EQUAL_SIGN | GREATER_OR_EQUAL | GREATER_THEN
  //     |LESS_OR_EQUAL | LESS_THEN | IN
  public static boolean relational_operator(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "relational_operator")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<relational operator>");
    result_ = consumeToken(builder_, EQUAL_SIGN);
    if (!result_) result_ = consumeToken(builder_, GREATER_OR_EQUAL);
    if (!result_) result_ = consumeToken(builder_, GREATER_THEN);
    if (!result_) result_ = consumeToken(builder_, LESS_OR_EQUAL);
    if (!result_) result_ = consumeToken(builder_, LESS_THEN);
    if (!result_) result_ = consumeToken(builder_, IN);
    exit_section_(builder_, level_, marker_, RELATIONAL_OPERATOR, result_, false, null);
    return result_;
  }

  /* ********************************************************** */
  // REPEAT statement (SEMICOLON statement)*
  //     UNTIL expression
  public static boolean repeat_statement(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "repeat_statement")) return false;
    if (!nextTokenIs(builder_, REPEAT)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, REPEAT);
    result_ = result_ && statement(builder_, level_ + 1);
    result_ = result_ && repeat_statement_2(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, UNTIL);
    result_ = result_ && expression(builder_, level_ + 1);
    exit_section_(builder_, marker_, REPEAT_STATEMENT, result_);
    return result_;
  }

  // (SEMICOLON statement)*
  private static boolean repeat_statement_2(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "repeat_statement_2")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!repeat_statement_2_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "repeat_statement_2", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // SEMICOLON statement
  private static boolean repeat_statement_2_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "repeat_statement_2_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, SEMICOLON);
    result_ = result_ && statement(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // while_statement | repeat_statement | for_statement
  public static boolean repetitive_statement(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "repetitive_statement")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<repetitive statement>");
    result_ = while_statement(builder_, level_ + 1);
    if (!result_) result_ = repeat_statement(builder_, level_ + 1);
    if (!result_) result_ = for_statement(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, REPETITIVE_STATEMENT, result_, false, null);
    return result_;
  }

  /* ********************************************************** */
  // type_identifier
  public static boolean result_type(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "result_type")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = type_identifier(builder_, level_ + 1);
    exit_section_(builder_, marker_, RESULT_TYPE, result_);
    return result_;
  }

  /* ********************************************************** */
  // LEFT_ROUND_BRACKET IDENTIFIER (COMMA IDENTIFIER)* RIGHT_ROUND_BRACKET
  public static boolean scalar_type(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "scalar_type")) return false;
    if (!nextTokenIs(builder_, LEFT_ROUND_BRACKET)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeTokens(builder_, 0, LEFT_ROUND_BRACKET, IDENTIFIER);
    result_ = result_ && scalar_type_2(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, RIGHT_ROUND_BRACKET);
    exit_section_(builder_, marker_, SCALAR_TYPE, result_);
    return result_;
  }

  // (COMMA IDENTIFIER)*
  private static boolean scalar_type_2(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "scalar_type_2")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!scalar_type_2_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "scalar_type_2", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // COMMA IDENTIFIER
  private static boolean scalar_type_2_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "scalar_type_2_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeTokens(builder_, 0, COMMA, IDENTIFIER);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // LEFT_SQUARE_BRACKET expression_list  RIGHT_SQUARE_BRACKET
  //     | LEFT_SQUARE_BRACKET RIGHT_SQUARE_BRACKET
  public static boolean set(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "set")) return false;
    if (!nextTokenIs(builder_, LEFT_SQUARE_BRACKET)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = set_0(builder_, level_ + 1);
    if (!result_) result_ = parseTokens(builder_, 0, LEFT_SQUARE_BRACKET, RIGHT_SQUARE_BRACKET);
    exit_section_(builder_, marker_, SET, result_);
    return result_;
  }

  // LEFT_SQUARE_BRACKET expression_list  RIGHT_SQUARE_BRACKET
  private static boolean set_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "set_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, LEFT_SQUARE_BRACKET);
    result_ = result_ && expression_list(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, RIGHT_SQUARE_BRACKET);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // SET_ OF base_type
  public static boolean set_type(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "set_type")) return false;
    if (!nextTokenIs(builder_, SET_)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeTokens(builder_, 0, SET_, OF);
    result_ = result_ && base_type(builder_, level_ + 1);
    exit_section_(builder_, marker_, SET_TYPE, result_);
    return result_;
  }

  /* ********************************************************** */
  // PLUS | MINUS
  static boolean sign(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "sign")) return false;
    if (!nextTokenIs(builder_, "", MINUS, PLUS)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, PLUS);
    if (!result_) result_ = consumeToken(builder_, MINUS);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // (term | adding_operator term) (adding_operator term)*
  public static boolean simple_expression(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "simple_expression")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<simple expression>");
    result_ = simple_expression_0(builder_, level_ + 1);
    result_ = result_ && simple_expression_1(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, SIMPLE_EXPRESSION, result_, false, null);
    return result_;
  }

  // term | adding_operator term
  private static boolean simple_expression_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "simple_expression_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = term(builder_, level_ + 1);
    if (!result_) result_ = simple_expression_0_1(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  // adding_operator term
  private static boolean simple_expression_0_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "simple_expression_0_1")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = adding_operator(builder_, level_ + 1);
    result_ = result_ && term(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  // (adding_operator term)*
  private static boolean simple_expression_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "simple_expression_1")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!simple_expression_1_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "simple_expression_1", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // adding_operator term
  private static boolean simple_expression_1_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "simple_expression_1_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = adding_operator(builder_, level_ + 1);
    result_ = result_ && term(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // assignment_statement | procedure_statement
  //     | goto_statement | empty
  public static boolean simple_statement(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "simple_statement")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<simple statement>");
    result_ = assignment_statement(builder_, level_ + 1);
    if (!result_) result_ = procedure_statement(builder_, level_ + 1);
    if (!result_) result_ = goto_statement(builder_, level_ + 1);
    if (!result_) result_ = empty(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, SIMPLE_STATEMENT, result_, false, null);
    return result_;
  }

  /* ********************************************************** */
  // scalar_type | subrange_type | type_identifier
  public static boolean simple_type(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "simple_type")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<simple type>");
    result_ = scalar_type(builder_, level_ + 1);
    if (!result_) result_ = subrange_type(builder_, level_ + 1);
    if (!result_) result_ = type_identifier(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, SIMPLE_TYPE, result_, false, null);
    return result_;
  }

  /* ********************************************************** */
  // label COLON unlabeled_statement | unlabeled_statement
  public static boolean statement(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "statement")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<statement>");
    result_ = statement_0(builder_, level_ + 1);
    if (!result_) result_ = unlabeled_statement(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, STATEMENT, result_, false, null);
    return result_;
  }

  // label COLON unlabeled_statement
  private static boolean statement_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "statement_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = label(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, COLON);
    result_ = result_ && unlabeled_statement(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // compound_statement
  public static boolean statement_part(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "statement_part")) return false;
    if (!nextTokenIs(builder_, BEGIN_)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = compound_statement(builder_, level_ + 1);
    exit_section_(builder_, marker_, STATEMENT_PART, result_);
    return result_;
  }

  /* ********************************************************** */
  // compound_statement | conditional_statement
  //     | repetitive_statement | with_statement
  public static boolean structured_statement(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "structured_statement")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<structured statement>");
    result_ = compound_statement(builder_, level_ + 1);
    if (!result_) result_ = conditional_statement(builder_, level_ + 1);
    if (!result_) result_ = repetitive_statement(builder_, level_ + 1);
    if (!result_) result_ = with_statement(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, STRUCTURED_STATEMENT, result_, false, null);
    return result_;
  }

  /* ********************************************************** */
  // unpacked_structured_type | PACKED_ unpacked_structured_type
  public static boolean structured_type(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "structured_type")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<structured type>");
    result_ = unpacked_structured_type(builder_, level_ + 1);
    if (!result_) result_ = structured_type_1(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, STRUCTURED_TYPE, result_, false, null);
    return result_;
  }

  // PACKED_ unpacked_structured_type
  private static boolean structured_type_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "structured_type_1")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, PACKED_);
    result_ = result_ && unpacked_structured_type(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // constant FULL_STOP FULL_STOP constant
  public static boolean subrange_type(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "subrange_type")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<subrange type>");
    result_ = constant(builder_, level_ + 1);
    result_ = result_ && consumeTokens(builder_, 0, FULL_STOP, FULL_STOP);
    result_ = result_ && constant(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, SUBRANGE_TYPE, result_, false, null);
    return result_;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean tag_field(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "tag_field")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, IDENTIFIER);
    exit_section_(builder_, marker_, TAG_FIELD, result_);
    return result_;
  }

  /* ********************************************************** */
  // factor (multiplying_operator factor)*
  public static boolean term(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "term")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<term>");
    result_ = factor(builder_, level_ + 1);
    result_ = result_ && term_1(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, TERM, result_, false, null);
    return result_;
  }

  // (multiplying_operator factor)*
  private static boolean term_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "term_1")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!term_1_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "term_1", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // multiplying_operator factor
  private static boolean term_1_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "term_1_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = multiplying_operator(builder_, level_ + 1);
    result_ = result_ && factor(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // simple_type | structured_type | pointer_type
  public static boolean type(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "type")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<type>");
    result_ = simple_type(builder_, level_ + 1);
    if (!result_) result_ = structured_type(builder_, level_ + 1);
    if (!result_) result_ = pointer_type(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, TYPE, result_, false, null);
    return result_;
  }

  /* ********************************************************** */
  // type_identifier EQUAL_SIGN type
  public static boolean type_definition(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "type_definition")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = type_identifier(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, EQUAL_SIGN);
    result_ = result_ && type(builder_, level_ + 1);
    exit_section_(builder_, marker_, TYPE_DEFINITION, result_);
    return result_;
  }

  /* ********************************************************** */
  // type_definition (SEMICOLON type_definition)*
  static boolean type_definition_list(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "type_definition_list")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = type_definition(builder_, level_ + 1);
    result_ = result_ && type_definition_list_1(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  // (SEMICOLON type_definition)*
  private static boolean type_definition_list_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "type_definition_list_1")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!type_definition_list_1_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "type_definition_list_1", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // SEMICOLON type_definition
  private static boolean type_definition_list_1_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "type_definition_list_1_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, SEMICOLON);
    result_ = result_ && type_definition(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // TYPE_ type_definition_list SEMICOLON | empty
  public static boolean type_definition_part(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "type_definition_part")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<type definition part>");
    result_ = type_definition_part_0(builder_, level_ + 1);
    if (!result_) result_ = empty(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, TYPE_DEFINITION_PART, result_, false, null);
    return result_;
  }

  // TYPE_ type_definition_list SEMICOLON
  private static boolean type_definition_part_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "type_definition_part_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, TYPE_);
    result_ = result_ && type_definition_list(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, SEMICOLON);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean type_identifier(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "type_identifier")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, IDENTIFIER);
    exit_section_(builder_, marker_, TYPE_IDENTIFIER, result_);
    return result_;
  }

  /* ********************************************************** */
  // structured_statement | simple_statement
  static boolean unlabeled_statement(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "unlabeled_statement")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = structured_statement(builder_, level_ + 1);
    if (!result_) result_ = simple_statement(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // array_type | record_type | set_type |file_type
  public static boolean unpacked_structured_type(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "unpacked_structured_type")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<unpacked structured type>");
    result_ = array_type(builder_, level_ + 1);
    if (!result_) result_ = record_type(builder_, level_ + 1);
    if (!result_) result_ = set_type(builder_, level_ + 1);
    if (!result_) result_ = file_type(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, UNPACKED_STRUCTURED_TYPE, result_, false, null);
    return result_;
  }

  /* ********************************************************** */
  // constant_identifier | unsigned_number | STRING | NIL
  static boolean unsigned_constant(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "unsigned_constant")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = constant_identifier(builder_, level_ + 1);
    if (!result_) result_ = unsigned_number(builder_, level_ + 1);
    if (!result_) result_ = consumeToken(builder_, STRING);
    if (!result_) result_ = consumeToken(builder_, NIL);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // UNSIGNED_REAL | UNSIGNED_INTEGER
  static boolean unsigned_number(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "unsigned_number")) return false;
    if (!nextTokenIs(builder_, "", UNSIGNED_INTEGER, UNSIGNED_REAL)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, UNSIGNED_REAL);
    if (!result_) result_ = consumeToken(builder_, UNSIGNED_INTEGER);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // variable_identifier variable_tail
  public static boolean variable(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "variable")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = variable_identifier(builder_, level_ + 1);
    result_ = result_ && variable_tail(builder_, level_ + 1);
    exit_section_(builder_, marker_, VARIABLE, result_);
    return result_;
  }

  /* ********************************************************** */
  // variable_identifier (COMMA variable_identifier)* COLON type
  public static boolean variable_declaration(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "variable_declaration")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = variable_identifier(builder_, level_ + 1);
    result_ = result_ && variable_declaration_1(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, COLON);
    result_ = result_ && type(builder_, level_ + 1);
    exit_section_(builder_, marker_, VARIABLE_DECLARATION, result_);
    return result_;
  }

  // (COMMA variable_identifier)*
  private static boolean variable_declaration_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "variable_declaration_1")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!variable_declaration_1_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "variable_declaration_1", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // COMMA variable_identifier
  private static boolean variable_declaration_1_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "variable_declaration_1_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, COMMA);
    result_ = result_ && variable_identifier(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // variable_declaration (SEMICOLON variable_declaration)*
  static boolean variable_declaration_list(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "variable_declaration_list")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = variable_declaration(builder_, level_ + 1);
    result_ = result_ && variable_declaration_list_1(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  // (SEMICOLON variable_declaration)*
  private static boolean variable_declaration_list_1(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "variable_declaration_list_1")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!variable_declaration_list_1_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "variable_declaration_list_1", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // SEMICOLON variable_declaration
  private static boolean variable_declaration_list_1_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "variable_declaration_list_1_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, SEMICOLON);
    result_ = result_ && variable_declaration(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // VAR_ variable_declaration_list SEMICOLON | empty
  public static boolean variable_declaration_part(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "variable_declaration_part")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<variable declaration part>");
    result_ = variable_declaration_part_0(builder_, level_ + 1);
    if (!result_) result_ = empty(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, VARIABLE_DECLARATION_PART, result_, false, null);
    return result_;
  }

  // VAR_ variable_declaration_list SEMICOLON
  private static boolean variable_declaration_part_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "variable_declaration_part_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, VAR_);
    result_ = result_ && variable_declaration_list(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, SEMICOLON);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean variable_identifier(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "variable_identifier")) return false;
    if (!nextTokenIs(builder_, IDENTIFIER)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, IDENTIFIER);
    exit_section_(builder_, marker_, VARIABLE_IDENTIFIER, result_);
    return result_;
  }

  /* ********************************************************** */
  // array_variable_tail | field_designator_tail
  //     | referenced_variable_tail | empty
  static boolean variable_tail(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "variable_tail")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = array_variable_tail(builder_, level_ + 1);
    if (!result_) result_ = field_designator_tail(builder_, level_ + 1);
    if (!result_) result_ = referenced_variable_tail(builder_, level_ + 1);
    if (!result_) result_ = empty(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // case_label_list COLON LEFT_ROUND_BRACKET field_list RIGHT_ROUND_BRACKET
  //     | case_label_list
  public static boolean variant(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "variant")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_, level_, _NONE_, "<variant>");
    result_ = variant_0(builder_, level_ + 1);
    if (!result_) result_ = case_label_list(builder_, level_ + 1);
    exit_section_(builder_, level_, marker_, VARIANT, result_, false, null);
    return result_;
  }

  // case_label_list COLON LEFT_ROUND_BRACKET field_list RIGHT_ROUND_BRACKET
  private static boolean variant_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "variant_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = case_label_list(builder_, level_ + 1);
    result_ = result_ && consumeTokens(builder_, 0, COLON, LEFT_ROUND_BRACKET);
    result_ = result_ && field_list(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, RIGHT_ROUND_BRACKET);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // CASE tag_field COLON type_identifier
  //     OF variant (SEMICOLON variant)*
  public static boolean variant_part(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "variant_part")) return false;
    if (!nextTokenIs(builder_, CASE)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, CASE);
    result_ = result_ && tag_field(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, COLON);
    result_ = result_ && type_identifier(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, OF);
    result_ = result_ && variant(builder_, level_ + 1);
    result_ = result_ && variant_part_6(builder_, level_ + 1);
    exit_section_(builder_, marker_, VARIANT_PART, result_);
    return result_;
  }

  // (SEMICOLON variant)*
  private static boolean variant_part_6(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "variant_part_6")) return false;
    int pos_ = current_position_(builder_);
    while (true) {
      if (!variant_part_6_0(builder_, level_ + 1)) break;
      if (!empty_element_parsed_guard_(builder_, "variant_part_6", pos_)) break;
      pos_ = current_position_(builder_);
    }
    return true;
  }

  // SEMICOLON variant
  private static boolean variant_part_6_0(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "variant_part_6_0")) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, SEMICOLON);
    result_ = result_ && variant(builder_, level_ + 1);
    exit_section_(builder_, marker_, null, result_);
    return result_;
  }

  /* ********************************************************** */
  // WHILE expression DO statement
  public static boolean while_statement(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "while_statement")) return false;
    if (!nextTokenIs(builder_, WHILE)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, WHILE);
    result_ = result_ && expression(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, DO);
    result_ = result_ && statement(builder_, level_ + 1);
    exit_section_(builder_, marker_, WHILE_STATEMENT, result_);
    return result_;
  }

  /* ********************************************************** */
  // WITH_ record_variable_list DO statement
  public static boolean with_statement(PsiBuilder builder_, int level_) {
    if (!recursion_guard_(builder_, level_, "with_statement")) return false;
    if (!nextTokenIs(builder_, WITH_)) return false;
    boolean result_ = false;
    Marker marker_ = enter_section_(builder_);
    result_ = consumeToken(builder_, WITH_);
    result_ = result_ && record_variable_list(builder_, level_ + 1);
    result_ = result_ && consumeToken(builder_, DO);
    result_ = result_ && statement(builder_, level_ + 1);
    exit_section_(builder_, marker_, WITH_STATEMENT, result_);
    return result_;
  }

}
