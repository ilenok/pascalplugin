package com.pascalplugin;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;
import com.pascalplugin.psi.PascalTypes;
import com.intellij.psi.TokenType;

%%

%class PascalLexer
%implements FlexLexer
%unicode
%ignorecase
%function advance
%type IElementType
%eof{
 return;
%eof}

WHITE_SPACE=[\ \t\f\n]
LETTER = [a-zA-Z]
DIGIT = [0-9]
LETTER_OR_DIGIT = {LETTER}|{DIGIT}

PLUS = "+"
MINUS = "-"
MULT = "*"
SLASH = "/"

AND = "and"
OR = "or"
NOT = "not"

EQUAL_SIGN = "="
GREATER_THEN = ">"
LESS_THEN = "<"
GREATER_OR_EQUAL = {GREATER_THEN}{EQUAL_SIGN}
LESS_OR_EQUAL = {LESS_THEN} {EQUAL_SIGN}

LEFT_ROUND_BRACKET = "("
RIGHT_ROUND_BRACkET = ")"
LEFT_SQUARE_BRACKET = "["
RIGHT_SQUARE_BRACKET = "]"
LEFT_CURLY_BRACKET = "{"
RIGHT_CURLY_BRACKET = "}"

ASSIGN = ":="
FULL_STOP = "."
COMMA = ","
SEMICOLON = ";"
COLON = ":"
DASH = "'"
CARET = "^"

DIV = "div"
MOD = "mod"
NIL = "nil"

IN = "in"
IF = "if"
THEN = "then"
ELSE = "else"
CASE = "case"
OF = "of"
REPEAT = "repeat"
UNTIL = "until"
WHILE = "while"
DO = "do"
FOR = "for"
TO = "to"
DOWNTO = "downto"

_BEGIN_ = "begin"
_END_ = "end"
_WITH_ = "with"
_GOTO_ = "goto"
_CONST_ = "const"
_VAR_ = "var"
_TYPE_ = "type"
_ARRAY_ = "array"
_RECORD_ = "record"
_SET_ = "set"
_FILE_ = "file"
_FUNCTION_ = "function"
_PROCEDURE_ = "procedure"
_LABEL_ = "label"
_PACKED_ = "packed"

COMMENT = {LEFT_CURLY_BRACKET}([^\{\}])+{RIGHT_CURLY_BRACKET}
STRING = {DASH}([^\'])*{DASH}

IDENTIFIER = {LETTER}({LETTER_OR_DIGIT})*

UNSIGNED_INTEGER = ({DIGIT})+
UNSIGNED_REAL = {UNSIGNED_INTEGER}"."{UNSIGNED_INTEGER}
    |{UNSIGNED_INTEGER}"."{UNSIGNED_INTEGER}+"E"{SCALE_FACTOR}|{UNSIGNED_INTEGER}"E"{SCALE_FACTOR}

SCALE_FACTOR = {UNSIGNED_INTEGER}|{SIGN} {UNSIGNED_INTEGER}
SIGN = "+"|"-"

%%

{COMMENT} {}

{STRING} { return PascalTypes.STRING; }

{PLUS} { return PascalTypes.PLUS; }
{MINUS} { return PascalTypes.MINUS; }

{MULT} { return PascalTypes.MULT; }
{SLASH} { return PascalTypes.SLASH; }

{AND} { return PascalTypes.AND; }
{OR} { return PascalTypes.OR; }
{NOT} { return PascalTypes.NOT; }

{EQUAL_SIGN} { return PascalTypes.EQUAL_SIGN; }

{GREATER_OR_EQUAL} { return PascalTypes.GREATER_OR_EQUAL; }
{GREATER_THEN} { return PascalTypes.GREATER_THEN; }
{LESS_OR_EQUAL} { return PascalTypes.LESS_OR_EQUAL; }
{LESS_THEN} { return PascalTypes.LESS_THEN; }

{LEFT_ROUND_BRACKET} { return PascalTypes.LEFT_ROUND_BRACKET; }
{RIGHT_ROUND_BRACkET} { return PascalTypes.RIGHT_ROUND_BRACKET; }

{LEFT_SQUARE_BRACKET} { return PascalTypes.LEFT_SQUARE_BRACKET; }
{RIGHT_SQUARE_BRACKET} { return PascalTypes.RIGHT_SQUARE_BRACKET; }

{ASSIGN} { return PascalTypes.ASSIGN; }

{FULL_STOP} { return PascalTypes.FULL_STOP; }
{COMMA} { return PascalTypes.COMMA; }
{SEMICOLON} { return PascalTypes.SEMICOLON; }
{COLON} { return PascalTypes.COLON; }

{CARET} { return PascalTypes.CARET; }

{DIV} { return PascalTypes.DIV; }
{MOD} { return PascalTypes.MOD;}

{NIL} { return PascalTypes.NIL; }

{IN} { return PascalTypes.IN; }

{IF} { return PascalTypes.IF; }
{THEN} { return PascalTypes.THEN; }
{ELSE} { return PascalTypes.ELSE; }

{CASE} { return PascalTypes.CASE; }
{OF} { return PascalTypes.OF; }

{REPEAT} { return PascalTypes.REPEAT; }
{UNTIL} { return PascalTypes.UNTIL; }

{WHILE} { return PascalTypes.WHILE; }
{DO} { return PascalTypes.DO; }

{FOR} { return PascalTypes.FOR; }
{TO} { return PascalTypes.TO; }
{DOWNTO} { return PascalTypes.DOWNTO; }

{_BEGIN_} { return PascalTypes.BEGIN_; }
{_END_} { return PascalTypes.END_; }

{_WITH_} { return PascalTypes.WITH_; }

{_GOTO_} { return PascalTypes.GOTO_; }

{_CONST_} { return PascalTypes.CONST_; }
{_VAR_} { return PascalTypes.VAR_; }
{_TYPE_} { return PascalTypes.TYPE_; }

{_ARRAY_}   { return PascalTypes.ARRAY_; }
{_RECORD_} { return PascalTypes.RECORD_; }
{_SET_} { return PascalTypes.SET_; }
{_FILE_} { return PascalTypes.FILE_; }

{_FUNCTION_} { return PascalTypes.FUNCTION_; }
{_PROCEDURE_} { return PascalTypes.PROCEDURE_; }

{_LABEL_} { return PascalTypes.LABEL_; }
{_PACKED_} { return PascalTypes.PACKED_; }


{IDENTIFIER}   { return PascalTypes.IDENTIFIER; }
{UNSIGNED_REAL}   { return PascalTypes.UNSIGNED_REAL; }
{UNSIGNED_INTEGER} { return PascalTypes.UNSIGNED_INTEGER; }

{WHITE_SPACE}+ {/*return TokenType.WHITE_SPACE; */ }

. {return TokenType.BAD_CHARACTER; }