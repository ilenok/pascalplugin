package com.pascalplugin;

import com.intellij.lexer.FlexAdapter;

import java.io.Reader;

/**
 * Created by ilenok on 4/27/14.
 */
public class PascalLexerAdapter extends FlexAdapter {
    public PascalLexerAdapter() {
        super(new PascalLexer((Reader) null));
    }
}
