package com.pascalplugin.psi;

/**
 * Created by ilenok on 3/9/14.
 */
import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import com.pascalplugin.PascalFileType;
import com.pascalplugin.PascalLanguage;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;


public class PascalFile extends PsiFileBase {
    public PascalFile(@NotNull FileViewProvider viewProvider) {
        super(viewProvider, PascalLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public FileType getFileType() {
        return PascalFileType.INSTANCE;
    }

    @Override
    public String toString() {
        return "Pascal File";
    }

    @Override
    public Icon getIcon(int flags) {
        return super.getIcon(flags);
    }
}
