package com.pascalplugin.psi;

/**
 * Created by ilenok on 3/8/14.
 */

import com.intellij.psi.tree.IElementType;
import com.pascalplugin.PascalLanguage;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public class PascalElementType extends IElementType {
    public PascalElementType(@NotNull @NonNls String debugName) {
        super(debugName, PascalLanguage.INSTANCE);
    }
}