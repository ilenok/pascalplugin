package com.pascalplugin;

/**
 * Created by ilenok on 3/8/14.
 */

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class PascalIcons {
    public static final Icon FILE
            = IconLoader.getIcon("/com/pascalplugin/icons/pascal.png");
}
