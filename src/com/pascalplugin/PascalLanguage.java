package com.pascalplugin;

/**
 * Created by ilenok on 3/10/14.
 */

import com.intellij.lang.Language;

public class PascalLanguage extends Language {
    public static final PascalLanguage INSTANCE = new PascalLanguage();

    private PascalLanguage() {
        super("Pascal");
    }
}
